// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.zip.ZipException;

/**
 * Represents an {@link InputStream} from a provided {@link File}: In case the
 * file points to a ZIP compressed file, then the uncompressed data of the
 * therein contained file with the same name excluding the ".zip" extension is
 * provided by the {@link InputStream}.
 */
public class ZipFileInputStream extends BufferedInputStream {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new zip file input stream impl.
	 *
	 * @param parent the parent
	 * @param child the child
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileInputStream( File parent, String child ) throws IOException {
		this( new File( parent, child ) );
	}

	/**
	 * Instantiates a new zip file input stream impl.
	 *
	 * @param parent the parent
	 * @param child the child
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileInputStream( String parent, String child ) throws IOException {
		this( new File( parent, child ) );
	}

	/**
	 * Instantiates a new zip file input stream impl.
	 *
	 * @param pathname the pathname
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileInputStream( String pathname ) throws IOException {
		this( new File( pathname ) );
	}

	/**
	 * Instantiates a new zip file input stream impl.
	 *
	 * @param uri the uri
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileInputStream( URI uri ) throws IOException {
		this( new File( uri ) );
	}

	/**
	 * Instantiates a new zip file input stream impl.
	 *
	 * @param aFile the file
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileInputStream( File aFile ) throws IOException {
		super( ZipUtility.toInputStream( aFile ) );
	}
}
