// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import org.refcodes.component.ConnectionStatus;

/**
 * The Class OutputStreamDatagramsTransmitter.
 *
 * @author steiner
 * 
 * @param <DATA> the generic type
 */
public class OutputStreamDatagramsTransmitter<DATA extends Serializable> implements DatagramsTransmitter<DATA> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final OutputStreamConnectionDatagramsTransmitter<DATA> _sender;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new output stream sender impl.
	 *
	 * @param aOutputStream the output stream
	 * 
	 * @throws IOException the open exception
	 */
	public OutputStreamDatagramsTransmitter( OutputStream aOutputStream ) throws IOException {
		_sender = new OutputStreamConnectionDatagramsTransmitter<>();
		_sender.open( aOutputStream );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA[] aDatagram ) throws IOException {
		_sender.transmit( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosed() {
		return _sender.isClosed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		return _sender.isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _sender.getConnectionStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA[] aDatagram, int aOffset, int aLength ) throws IOException {
		_sender.transmit( aDatagram, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_sender.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA aDatagram ) throws IOException {
		_sender.transmit( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_sender.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosable() {
		return _sender.isClosable();
	}
}
