// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.OutputStream;

import org.refcodes.numerical.NumericalUtility;
import org.refcodes.runtime.SystemProperty;

/**
 * A {@link HexOutputStream} wraps an {@link OutputStream} printing out the
 * bytes written to the {@link HexOutputStream} as hexadecimal text to the
 * wrapped {@link OutputStream}. This is most useful in conjunction with either
 * the {@link OutputStreamComposite} or the {@link InputStreamTap} to sniff data
 * written to / read from the according stream while still forwarding the calls
 * to original streams.
 */
public class HexOutputStream extends OutputStream {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final byte[] HEX_SPACE = " ".getBytes();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private long _count = 0;
	private final String _lineBreak;
	private final int _lineLength;
	private final OutputStream _outputStream;
	private final boolean _isPrefixBytes;
	private final boolean _isFlushLineBreak;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link HexOutputStream} with the given arguments used for
	 * configuration. The line break characters used are the ones used by the
	 * underlying operating system.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the bytes
	 *        converted to hexadecimal text to.
	 */
	public HexOutputStream( OutputStream aOutputStream ) {
		_lineBreak = SystemProperty.LINE_SEPARATOR.getValue();
		_lineLength = -1;
		_outputStream = aOutputStream;
		_isPrefixBytes = true;
		_isFlushLineBreak = false;
	}

	/**
	 * Constructs a {@link HexOutputStream} with the given arguments used for
	 * configuration.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write the bytes
	 *        converted to hexadecimal text to.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 */
	public HexOutputStream( OutputStream aOutputStream, boolean isPrefixBytes ) {
		_lineBreak = SystemProperty.LINE_SEPARATOR.getValue();
		_lineLength = -1;
		_outputStream = aOutputStream;
		_isPrefixBytes = isPrefixBytes;
		_isFlushLineBreak = false;
	}

	/**
	 * Constructs a {@link HexOutputStream} with the given arguments used for
	 * configuration.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write the bytes
	 *        converted to hexadecimal text to.
	 * @param aLineLength The number of bytes after which to insert a line
	 *        break, a line length of -1 does not insert any line breaks at all.
	 * @param aLineBreak The line break chars to use.
	 */
	public HexOutputStream( OutputStream aOutputStream, int aLineLength, String aLineBreak ) {
		_lineBreak = aLineBreak;
		_lineLength = aLineLength;
		_outputStream = aOutputStream;
		_isPrefixBytes = true;
		_isFlushLineBreak = false;
	}

	/**
	 * Constructs a {@link HexOutputStream} with the given arguments used for
	 * configuration.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the bytes
	 *        converted to hexadecimal text to.
	 * @param aLineLength The number of bytes after which to insert a line
	 *        break, a line length of -1 does not insert any line breaks at all.
	 * @param aLineBreak The line break chars to use.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 */
	public HexOutputStream( OutputStream aOutputStream, int aLineLength, String aLineBreak, boolean isPrefixBytes ) {
		_lineBreak = aLineBreak;
		_lineLength = aLineLength;
		_outputStream = aOutputStream;
		_isPrefixBytes = isPrefixBytes;
		_isFlushLineBreak = false;
	}

	/**
	 * Constructs a {@link HexOutputStream} with the given arguments used for
	 * configuration. The line break characters used are the ones used by the
	 * underlying operating system.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the bytes
	 *        converted to hexadecimal text to.
	 * @param aLineLength The number of bytes after which to insert a line
	 *        break, a line length of -1 does not insert any line breaks at all.
	 */
	public HexOutputStream( OutputStream aOutputStream, int aLineLength ) {
		_lineBreak = SystemProperty.LINE_SEPARATOR.getValue();
		_lineLength = aLineLength;
		_outputStream = aOutputStream;
		_isPrefixBytes = true;
		_isFlushLineBreak = false;
	}

	/**
	 * Constructs a {@link HexOutputStream} with the given arguments used for
	 * configuration. The line break characters used are the ones used by the
	 * underlying operating system.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the bytes
	 *        converted to hexadecimal text to.
	 * @param aLineLength The number of bytes after which to insert a line
	 *        break, a line length of -1 does not insert any line breaks at all.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 */
	public HexOutputStream( OutputStream aOutputStream, int aLineLength, boolean isPrefixBytes ) {
		_lineBreak = SystemProperty.LINE_SEPARATOR.getValue();
		_lineLength = aLineLength;
		_outputStream = aOutputStream;
		_isPrefixBytes = isPrefixBytes;
		_isFlushLineBreak = false;
	}

	/**
	 * Constructs a {@link HexOutputStream} with the given arguments used for
	 * configuration.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write the bytes
	 *        converted to hexadecimal text to.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * @param isFlushLineBreak When true, then a line break is also inserted
	 *        upon a {@link #flush()} operation.
	 */
	public HexOutputStream( OutputStream aOutputStream, boolean isPrefixBytes, boolean isFlushLineBreak ) {
		_lineBreak = SystemProperty.LINE_SEPARATOR.getValue();
		_lineLength = -1;
		_outputStream = aOutputStream;
		_isPrefixBytes = isPrefixBytes;
		_isFlushLineBreak = isFlushLineBreak;
	}

	/**
	 * Constructs a {@link HexOutputStream} with the given arguments used for
	 * configuration.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the bytes
	 *        converted to hexadecimal text to.
	 * @param aLineLength The number of bytes after which to insert a line
	 *        break, a line length of -1 does not insert any line breaks at all.
	 * @param aLineBreak The line break chars to use.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * @param isFlushLineBreak When true, then a line break is also inserted
	 *        upon a {@link #flush()} operation.
	 */
	public HexOutputStream( OutputStream aOutputStream, int aLineLength, String aLineBreak, boolean isPrefixBytes, boolean isFlushLineBreak ) {
		_lineBreak = aLineBreak;
		_lineLength = aLineLength;
		_outputStream = aOutputStream;
		_isPrefixBytes = isPrefixBytes;
		_isFlushLineBreak = isFlushLineBreak;
	}

	/**
	 * Constructs a {@link HexOutputStream} with the given arguments used for
	 * configuration. The line break characters used are the ones used by the
	 * underlying operating system.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the bytes
	 *        converted to hexadecimal text to.
	 * @param aLineLength The number of bytes after which to insert a line
	 *        break, a line length of -1 does not insert any line breaks at all.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * @param isFlushLineBreak When true, then a line break is also inserted
	 *        upon a {@link #flush()} operation.
	 */
	public HexOutputStream( OutputStream aOutputStream, int aLineLength, boolean isPrefixBytes, boolean isFlushLineBreak ) {
		_lineBreak = SystemProperty.LINE_SEPARATOR.getValue();
		_lineLength = aLineLength;
		_outputStream = aOutputStream;
		_isPrefixBytes = isPrefixBytes;
		_isFlushLineBreak = isFlushLineBreak;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( int aByte ) throws IOException {
		_count++;
		String theHexString = NumericalUtility.toHexString( aByte, _isPrefixBytes );
		_outputStream.write( theHexString.getBytes() );
		if ( ( _lineLength != -1 ) && ( _count % _lineLength == 0 ) ) {
			_outputStream.write( _lineBreak.getBytes() );
			_count = 0;
		}
		else {
			_outputStream.write( HEX_SPACE );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		if ( _isFlushLineBreak ) {
			_outputStream.write( _lineBreak.getBytes() );
			_count = 0;
		}
		super.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( _lineLength != -1 ) {
			if ( _count % _lineLength != 0 ) {
				_outputStream.write( _lineBreak.getBytes() );
				_count = 0;
			}
			super.close();
		}
	}
}
