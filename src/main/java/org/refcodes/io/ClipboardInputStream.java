// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A {@link ClipboardInputStream} provides the clipboard as {@link InputStream}.
 */
public class ClipboardInputStream extends ByteArrayInputStream {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an {@link InputStream} from the clipboard's textual content.
	 * 
	 * @throws UnsupportedFlavorException thrown in case there is no textual
	 *         content.
	 * @throws IOException thrown in case of issues accessing the clipboard.
	 */
	public ClipboardInputStream() throws UnsupportedFlavorException, IOException {
		super( getClipboard() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static byte[] getClipboard() throws UnsupportedFlavorException, IOException {
		final Clipboard theClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		final String theSelection = ( (String) theClipboard.getData( DataFlavor.stringFlavor ) );
		return theSelection != null ? theSelection.getBytes() : new byte[] {};
	}
}
