package org.refcodes.io;

import java.io.IOException;
import java.util.List;

import org.refcodes.struct.ByteArrayAccessor;

/**
 * The Class ByteArrayReceiver.
 */
public class ByteArrayReceiver extends AbstractBytesReceiver implements ByteArrayAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new byte array receiver impl.
	 *
	 * @param aDatagrams the datagrams
	 */
	public ByteArrayReceiver( byte[] aDatagrams ) {
		super( aDatagrams.length );
		try {
			open();
			pushDatagrams( aDatagrams );
		}
		catch ( IOException ignore ) {}
	}

	/**
	 * Instantiates a new byte array receiver impl.
	 *
	 * @param aDatagrams the datagrams
	 */
	public ByteArrayReceiver( List<Byte> aDatagrams ) {
		this( toPrimitiveType( aDatagrams.toArray( new Byte[aDatagrams.size()] ) ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getBytes() {
		return toPrimitiveType( _datagramQueue.toArray( new Byte[_datagramQueue.size()] ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static byte[] toPrimitiveType( Byte[] aBytes ) {
		if ( aBytes == null ) {
			return null;
		}
		final byte[] thePrimitives = new byte[aBytes.length];
		for ( int i = 0; i < aBytes.length; i++ ) {
			thePrimitives[i] = aBytes[i].byteValue();
		}
		return thePrimitives;
	}
}
