// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Array;

/**
 * The {@link DatagramsSource} is used to send datagram blocks (arrays) in a
 * unified way.
 *
 * @param <DATA> The type of the datagram block (array) to be operated with. Do
 *        not provide an array type as the methods use to generic type for
 *        defining an array argument.
 */
@FunctionalInterface
public interface DatagramsSource<DATA extends Serializable> extends DatagramSource<DATA> {

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default void transmit( DATA aDatagram ) throws IOException {
		if ( aDatagram != null ) {
			final DATA[] theArray = (DATA[]) Array.newInstance( aDatagram.getClass(), 1 );
			theArray[0] = aDatagram;
			transmit( theArray, 0, 1 );
		}
	}

	/**
	 * Writes (sends) a datagram block to a listening {@link DatagramReceiver}
	 * or {@link DatagramsReceiver}.
	 * 
	 * @param aDatagram The datagram to be pushed to the receiving
	 *        {@link DatagramReceiver} or {@link DatagramsReceiver}.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	default void transmit( DATA[] aDatagram ) throws IOException {
		transmit( aDatagram, 0, aDatagram.length );
	}

	/**
	 * Write datagrams.
	 *
	 * @param aDatagram the datagram
	 * @param aOffset the offset
	 * @param aLength the length
	 * 
	 * @throws IOException the open exception
	 */
	void transmit( DATA[] aDatagram, int aOffset, int aLength ) throws IOException;

}
