// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Makes the wrapped {@link ListIterator} serializable, else passing
 * {@link ListIterator} instances would cause an
 * {@link NotSerializableException} or similar.
 *
 * @param <T> The type of the instances managed by the {@link ListIterator}.
 */
public class SerializableListIterator<T> implements ListIterator<T>, Serializable {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private List<T> _list = new ArrayList<>();

	private transient ListIterator<T> _listIterator = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new serializable list iterator impl.
	 */
	public SerializableListIterator() {}

	/**
	 * Instantiates a new serializable list iterator impl.
	 *
	 * @param aListIterator the list iterator
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SerializableListIterator( ListIterator<T> aListIterator ) {
		assert ( aListIterator != null );
		T eObj;
		while ( aListIterator.hasNext() ) {
			eObj = aListIterator.next();
			if ( ( eObj instanceof ListIterator ) && ( !( eObj instanceof SerializableListIterator ) ) ) {
				eObj = (T) new SerializableListIterator( (ListIterator) eObj );
			}

			else if ( ( eObj instanceof Iterator ) && ( !( eObj instanceof SerializableIterator ) ) ) {
				eObj = (T) new SerializableIterator( (Iterator) eObj );
			}
			_list.add( eObj );
		}
		_listIterator = _list.listIterator();
		_list = null;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add( T value ) {
		_listIterator.add( value );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return _listIterator.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPrevious() {
		return _listIterator.hasPrevious();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() {
		return _listIterator.next();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int nextIndex() {
		return _listIterator.nextIndex();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T previous() {
		return _listIterator.previous();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int previousIndex() {
		return _listIterator.previousIndex();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		_listIterator.remove();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void set( T value ) {
		_listIterator.set( value );
	}
}
