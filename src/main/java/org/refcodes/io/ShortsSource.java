// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;

/**
 * The {@link ShortsSource} is used to send short blocks (arrays) in a unified
 * way.
 */
@FunctionalInterface
public interface ShortsSource extends ShortSource {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void transmitShort( short aShort ) throws IOException {
		transmitShorts( new short[] { aShort }, 0, 1 );
	}

	/**
	 * Writes (sends) a short block.
	 * 
	 * @param aShorts The short to be pushed.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	default void transmitShorts( short[] aShorts ) throws IOException {
		transmitShorts( aShorts, 0, aShorts.length );
	}

	/**
	 * Writes (sends) a short block.
	 * 
	 * @param aShorts The short to be pushed.
	 * @param aOffset The offset from which to take the data.
	 * @param aLength The number of elements to push starting at the given
	 *        offset.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	void transmitShorts( short[] aShorts, int aOffset, int aLength ) throws IOException;

}
