// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.LinkedList;

import org.refcodes.component.AbstractConnectableAutomaton;
import org.refcodes.component.ConnectionComponent.ConnectionAutomaton;
import org.refcodes.component.ConnectionOpenable;
import org.refcodes.component.ConnectionStatus;

/**
 * Abstract implementation of the {@link DatagramsReceiver} interface. The
 * {@link #open(InputStream)}, {@link #setConnectionStatus(ConnectionStatus)}
 * and {@link #isOpenable()} methods are your hooks when extending this class.
 *
 * @param <DATA> The type of the datagram to be operated with.
 */
public abstract class AbstractInputStreamReceiver<DATA extends Serializable> extends AbstractConnectableAutomaton implements DatagramsReceiver<DATA> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final LinkedList<DATA> _datagramQueue = new LinkedList<>();

	private ObjectInputStream _inputStream = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Checks for datagram.
	 *
	 * @return true, if successful
	 * 
	 * @throws IOException the open exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public int available() throws IOException {
		if ( !_datagramQueue.isEmpty() ) {
			return _datagramQueue.size();
		}
		if ( isClosed() ) {
			return 0;
		}
		try {
			final Object theObject = _inputStream.readObject();
			if ( theObject == null ) {
				if ( isClosed() ) {
					return 0;
				}
				close();
				synchronized ( _datagramQueue ) {
					_datagramQueue.notifyAll();
				}
				return 0;
			}
			_datagramQueue.add( (DATA) theObject );
			return _datagramQueue.size();
		}
		//	catch ( IOException e ) {
		//		synchronized ( _datagramQueue ) {
		//			_datagramQueue.notifyAll();
		//		}
		//		throw new IOException( "Unable to test datagram availability (connection status is <" + getConnectionStatus() + ">).", e );
		//	}
		catch ( IOException ioe ) {

			if ( isClosed() ) {
				return 0;
			}

			synchronized ( _datagramQueue ) {
				_datagramQueue.notifyAll();
			}
			try {
				if ( isThrownAsOfAlreadyClosed( ioe ) ) {
					super.close();
				}
				else {
					close();
				}
			}
			catch ( IOException e ) {
				throw new IOException( "Unable to test datagram availability (connection status is <" + getConnectionStatus() + ">).", e );
			}
			throw new IOException( "Unable to test datagram availability (connection status is <" + getConnectionStatus() + ">).", ioe );
		}
		catch ( ClassNotFoundException e ) {
			throw new IOException( "Datagram read is of unknown type (connection status is <" + getConnectionStatus() + ">).", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DATA receive() throws IOException {
		if ( _datagramQueue.isEmpty() && !isOpened() ) {
			throw new IOException( "Unable to read datagram  as the connection is NOT OPEN; connection status is <" + getConnectionStatus() + ">." );
		}
		if ( !hasAvailable() ) {
			throw new IOException( "Unable to read datagram  as the connection is NOT OPEN; connection status is <" + getConnectionStatus() + ">." );
		}
		final DATA theDatagram = _datagramQueue.poll();
		while ( theDatagram == null ) {
			if ( !hasAvailable() ) {
				throw new IOException( "Unable to read datagram  as the connection is NOT OPEN; connection status is <" + getConnectionStatus() + ">." );
			}
		}
		return theDatagram;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void close() throws IOException {
		if ( !isClosed() ) {
			super.close();
			try {
				_inputStream.close();
			}
			catch ( IOException e ) {
				if ( !isThrownAsOfAlreadyClosed( e ) ) {
					throw new IOException( "Unable to close receiver (connection status is <" + getConnectionStatus() + ">).", e );
				}
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Open, see also {@link ConnectionOpenable#open(Object)}.
	 *
	 * @param aInputStream the input stream
	 * 
	 * @throws IOException the open exception
	 */
	protected synchronized void open( InputStream aInputStream ) throws IOException {
		if ( isOpened() ) {
			throw new IOException( "Unable to open the connection is is is ALREADY OPEN; connection status is " + getConnectionStatus() + "." );
		}
		try {
			if ( !( aInputStream instanceof BufferedInputStream ) ) {
				_inputStream = new SerializableObjectInputStream( new BufferedInputStream( aInputStream ) );
			}
			else {
				_inputStream = new SerializableObjectInputStream( aInputStream );
			}
		}
		catch ( IOException aException ) {
			throw new IOException( "Unable to open the I/O stream receiver as of a causing exception.", aException );
		}
		setConnectionStatus( ConnectionStatus.OPENED );
	}

	/**
	 * Checks if is openable. See also
	 * {@link ConnectionAutomaton#isOpenable(Object)}.
	 *
	 * @param aInputStream the input stream
	 * 
	 * @return true, if is openable
	 */
	protected boolean isOpenable( InputStream aInputStream ) {
		return aInputStream == null ? false : !isOpened();
	}
}
