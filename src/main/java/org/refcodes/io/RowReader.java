// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * A {@link RowReader} reads a set of elements (a row) from an external source.
 */
public interface RowReader<T> extends AutoCloseable {

	/**
	 * Determines whether a next element can be read.
	 * 
	 * @return True in case there is a next element, false if not.
	 */
	boolean hasNext();

	/**
	 * Reads the next element.
	 * 
	 * @return The next element being read.
	 * 
	 * @throws IOException thrown in case there was an I/O related problem.
	 * @throws NoSuchElementException thrown in case there is none next element
	 *         to be read (check with {@link #hasNext()} if there is actually a
	 *         next element which can be read).
	 */
	T nextRow() throws IOException;

	/**
	 * Reads the next row and returns it as is ("raw"). This is useful when
	 * encountering a conversion problem or format problem regarding one line in
	 * a CSV file and this erroneous line is to be addressed separately.
	 * 
	 * @return The next raw line "as is".
	 */
	String nextRaw();

}
