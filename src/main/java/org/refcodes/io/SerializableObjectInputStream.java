// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.io.StreamCorruptedException;

/**
 * The {@link SerializableObjectInputStream} extends an ordinary
 * {@link ObjectInputStream} to also resolve primitive types. In former JDKs
 * (not tested with current ones), primitive types caused problems when
 * deserializing. This class addresses and patches this issue.
 * <p>
 * Interesting enough that there were already virtual class definitions for the
 * primitive types ("boolean.class" - {@link Boolean#TYPE}, "byte.class" -
 * {@link Byte#TYPE}, "char.clas" - {@link Character#TYPE}, "int.class" -
 * {@link Integer#TYPE}, "double.class" - {@link Double#TYPE}, "long.class" -
 * {@link Long#TYPE}, float.class" - {@link Float#TYPE}).
 */
public class SerializableObjectInputStream extends ObjectInputStream {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new serializable object input stream impl.
	 *
	 * @param in the in
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws StreamCorruptedException the stream corrupted exception
	 */
	public SerializableObjectInputStream( InputStream in ) throws IOException {
		super( in );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<?> resolveClass( ObjectStreamClass aClass ) throws ClassNotFoundException, IOException {
		try {
			return super.resolveClass( aClass );
		}
		catch ( ClassNotFoundException aException ) {
			if ( "boolean".equals( aClass.getName() ) ) {
				return boolean.class;
			}
			else if ( "byte".equals( aClass.getName() ) ) {
				return byte.class;
			}
			else if ( "char".equals( aClass.getName() ) ) {
				return char.class;
			}
			else if ( "double".equals( aClass.getName() ) ) {
				return double.class;
			}
			else if ( "float".equals( aClass.getName() ) ) {
				return float.class;
			}
			else if ( "int".equals( aClass.getName() ) ) {
				return int.class;
			}
			else if ( "long".equals( aClass.getName() ) ) {
				return long.class;
			}
			else if ( "short".equals( aClass.getName() ) ) {
				return short.class;
			}
			else if ( "void".equals( aClass.getName() ) ) {
				return void.class;
			}
			throw aException;
		}
	}
}
