// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.EOFException;
import java.io.IOException;

/**
 * The {@link ShortDestination} is used to receive shorts in a unified way. The
 * {@link #receiveShort()} method provides the next available short from the
 * counterpart {@link DatagramTransmitter}; in case there is none available,
 * then this method halts until one is available.
 */
@FunctionalInterface
public interface ShortDestination {

	/**
	 * Reads (receives) the next short available, in case none short is
	 * available, then this method blocks until one is available.
	 * 
	 * @return The next short sent from the {@link ShortsTransmitter}
	 *         counterpart.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	short receiveShort() throws IOException;
}
