// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.refcodes.data.SleepLoopTime;

/**
 * The Interface DatagramsReceiver.
 *
 * @param <DATA> the generic type
 */
public interface DatagramsReceiver<DATA extends Serializable> extends DatagramReceiver<DATA>, DatagramsDestination<DATA> {

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default DATA[] receiveAll() throws IOException {
		final List<DATA> theData = new ArrayList<>();
		while ( hasAvailable() ) {
			theData.add( receive() );
		}
		return theData.toArray( ( (DATA[]) new Object[theData.size()] ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default DATA[] receive( int aLength ) throws IOException {
		final DATA[] theBlock = (DATA[]) new Object[aLength];
		int i = 0;
		while ( hasAvailable() && i < aLength ) {
			theBlock[i] = receive();
			i++;
		}
		if ( i == 0 ) {
			while ( !hasAvailable() ) {
				try {
					Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
				}
				catch ( InterruptedException ignore ) {}
				while ( hasAvailable() && i < aLength ) {
					theBlock[i] = receive();
					i++;
				}
			}
		}
		if ( i == aLength ) {
			return theBlock;
		}
		return Arrays.copyOfRange( theBlock, 0, i );
	}
}
