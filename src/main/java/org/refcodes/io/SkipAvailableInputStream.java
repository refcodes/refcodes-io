// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.exception.TimeoutIOException;
import org.refcodes.mixin.ReadTimeoutMillisAccessor;

/**
 * The {@link SkipAvailableInputStream} decorates an {@link InputStream} with
 * skip timed functionality, e.g. skip all currently available bytes
 * ({@link #skipAvailable()}), all bytes available for a period of time
 * ({@link #skipAvailableWithin(long)}) or skip all bytes available till a send
 * pause of a given time is detected ({@link #skipAvailableTillSilenceFor(long)}
 * or {@link #skipAvailableTillSilenceFor(long, long)}).
 * Ehttps://www.metacodes.proly the latter two methods are useful when a send
 * pause is used by some kind of handshake mechanism. ATTENTION: This
 * implementation relies upon the {@link #available()} method to return some
 * useful information!
 */
public class SkipAvailableInputStream extends InputStream implements ReadTimeoutMillisAccessor, Skippable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private InputStream _inputStream;
	private long _readTimeoutInMs;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link SkipAvailableInputStream} decorating an
	 * {@link InputStream} with additional timeout functionality.
	 * 
	 * @param aInputStream The {@link InputStream} to be decorated.
	 */
	public SkipAvailableInputStream( InputStream aInputStream ) {
		this( aInputStream, -1 );
	}

	/**
	 * Constructs a {@link SkipAvailableInputStream} decorating an
	 * {@link InputStream} with additional timeout functionality.
	 * 
	 * @param aInputStream The {@link InputStream} to be decorated.
	 * @param aTimeoutMillis The default timeout for read operations not
	 *        explicitly called with a timeout argument. With a value of -1
	 *        timeout handling is disabled (blocking mode).
	 */
	public SkipAvailableInputStream( InputStream aInputStream, long aTimeoutMillis ) {
		_inputStream = aInputStream;
		_readTimeoutInMs = aTimeoutMillis;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc} In case the read timeout as of
	 * {@link #getReadTimeoutMillis()}) is not -1 and the overall time while
	 * skipping bytes exceeds the read timeout a {@link TimeoutIOException} is
	 * thrown. ATTENTION: To apply a custom timeout, please use
	 * {@link #skipAvailableTillSilenceFor(long, long)}.
	 * 
	 * @throws TimeoutIOException in case the the read timeout is not -1 as of
	 *         {@link #getReadTimeoutMillis()} and the overall time while
	 *         skipping bytes exceeds the read timeout .
	 */
	@Override
	public void skipAvailableTillSilenceFor( long aSilenceTimeSpanInMs ) throws IOException {
		skipAvailableTillSilenceFor( aSilenceTimeSpanInMs, getReadTimeoutMillis() );
	}

	/**
	 * {@inheritDoc} In case the read timeout as of
	 * {@link #getReadTimeoutMillis()}) is not -1 and the overall time while
	 * skipping bytes exceeds the read timeout a {@link TimeoutIOException} is
	 * thrown. ATTENTION: To apply a custom timeout, please use
	 * {@link #skipAvailableWithin(long, long)}.
	 * 
	 * @throws TimeoutIOException in case the the read timeout is not -1 as of
	 *         {@link #getReadTimeoutMillis()} and the overall time while
	 *         skipping bytes exceeds the read timeout .
	 */
	@Override
	public void skipAvailableWithin( long aSkipTimeSpanInMs ) throws IOException {
		skipAvailableWithin( aSkipTimeSpanInMs, getReadTimeoutMillis() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _inputStream.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException {
		return _inputStream.read();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] aB, int aOff, int aLen ) throws IOException {
		return _inputStream.read( aB, aOff, aLen );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] aB ) throws IOException {
		return _inputStream.read( aB );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] readAllBytes() throws IOException {
		return _inputStream.readAllBytes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int readNBytes( byte[] b, int off, int len ) throws IOException {
		return _inputStream.readNBytes( b, off, len );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] readNBytes( int aArg0 ) throws IOException {
		return _inputStream.readNBytes( aArg0 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _inputStream.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long transferTo( OutputStream aOut ) throws IOException {
		return _inputStream.transferTo( aOut );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_inputStream.close();
		super.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mark( int readlimit ) {
		_inputStream.mark( readlimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported() {
		return _inputStream.markSupported();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() throws IOException {
		_inputStream.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip( long n ) throws IOException {
		return _inputStream.skip( n );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getReadTimeoutMillis() {
		return _readTimeoutInMs;
	}
}
