// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.OutputStream;

import org.refcodes.mixin.OutputStreamAccessor;

/**
 * The Interface BytesTransmitter.
 */
public interface BytesTransmitter extends BytesSource, ByteTransmitter, OutputStreamAccessor {
	/**
	 * {@inheritDoc}
	 */
	@Override
	default void transmitBytes( byte[] aBytes, int aOffset, int aLength ) throws IOException {
		for ( int i = 0; i < aLength; i++ ) {
			transmitByte( aBytes[aOffset + i] );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default OutputStream getOutputStream() {
		return new TransmitterOutputStream( this );
	}

	/**
	 * The {@link TransmitterOutputStream} constructs an {@link OutputStream}
	 * from a {@link BytesTransmitter}.
	 */
	static class TransmitterOutputStream extends OutputStream {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final BytesTransmitter _transmitter;
		private boolean _isClosed = false;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Uses the provided {@link BytesTransmitter} to provide
		 * {@link OutputStream} functionality.
		 * 
		 * @param aBytesTransmitter The transmitter to use.
		 */
		public TransmitterOutputStream( BytesTransmitter aBytesTransmitter ) {
			_transmitter = aBytesTransmitter;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void write( int aByte ) throws IOException {
			if ( _isClosed ) {
				throw new IOException( "The stream has already been closed!" );
			}
			_transmitter.transmitByte( (byte) aByte );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void write( byte[] aBytes, int aOffset, int aLength ) throws IOException {
			if ( _isClosed ) {
				throw new IOException( "The stream has already been closed!" );
			}
			_transmitter.transmitBytes( aBytes, aOffset, aLength );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void close() throws IOException {
			_isClosed = true;
			super.close();
		}
	}
}
