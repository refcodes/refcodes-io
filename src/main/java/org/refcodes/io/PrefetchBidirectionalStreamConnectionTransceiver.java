// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.Serializable;
import java.util.concurrent.ExecutorService;

/**
 * The Class PrefetchBidirectionalStreamConnectionTransceiver.
 *
 * @author steiner
 * 
 * @param <DATA> the generic type
 */
public class PrefetchBidirectionalStreamConnectionTransceiver<DATA extends Serializable> extends AbstractPrefetchInputStreamReceiver<DATA> implements BidirectionalStreamConnectionTransceiver<DATA> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ObjectOutputStream _objectOutputStream = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new prefetch bidirectional stream connection transceiver
	 * impl.
	 */
	public PrefetchBidirectionalStreamConnectionTransceiver() {}

	/**
	 * Instantiates a new prefetch bidirectional stream connection transceiver
	 * impl.
	 *
	 * @param aExecutorService the executor service
	 */
	public PrefetchBidirectionalStreamConnectionTransceiver( ExecutorService aExecutorService ) {
		super( aExecutorService );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable( InputStream aInputStream, OutputStream aOutputStream ) {
		if ( aOutputStream == null ) {
			return false;
		}
		return isOpenable( aInputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( InputStream aInputStream, OutputStream aOutputStream ) throws IOException {
		open( aOutputStream );
		open( aInputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA aDatagram ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Unable to write datagram <" + aDatagram.getClass().getName() + "> as the connection is NOT OPEN; connection status is " + getConnectionStatus() + "." );
		}
		synchronized ( _objectOutputStream ) {
			try {
				_objectOutputStream.writeObject( aDatagram );
				_objectOutputStream.flush();
			}
			catch ( IOException aException ) {
				if ( isClosed() ) {
					return;
				}
				else {
					try {
						close();
					}
					catch ( IOException e ) {
						throw new IOException( "Unable to close malfunctioning stream.", e );
					}
				}
				throw new IOException( aException );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		try {
			_objectOutputStream.flush();
		}
		catch ( IOException e ) {
			throw new IOException( "Unable to flush transceiver's output stream <" + _objectOutputStream + ">.", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void close() throws IOException {
		super.close();
		try {
			_objectOutputStream.close();
		}
		catch ( IOException aException ) {
			if ( !isThrownAsOfAlreadyClosed( aException ) ) {
				throw new IOException( "Unable to close stream <" + _objectOutputStream + ">.", aException );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * HINT: For testing purposes you can pass an already flushed
	 * {@link ObjectOutputStream} (via {@link ObjectOutputStream#flush()})
	 * encapsulating a {@link PipedOutputStream} which in turn encapsulates a
	 * {@link PipedInputStream} to this {@link #open(OutputStream)} method. This
	 * enables you a non-blocking test of the
	 * {@link OutputStreamConnectionDatagramsTransmitter} connected with the
	 * {@link PrefetchInputStreamConnectionReceiver} counterpart which is being
	 * opened with the {@link PipedInputStream}. ATTENTION: For maintainers,
	 * please keep the below code in sync with the code from
	 * {@link OutputStreamConnectionDatagramsTransmitter#open(java.io.OutputStream)};
	 * except do not set the connection status here, make it protected and do
	 * not @Override anything.
	 *
	 * @param aOutputStream the output stream
	 * 
	 * @throws IOException the open exception
	 */
	protected synchronized void open( OutputStream aOutputStream ) throws IOException {
		if ( isOpened() ) {
			throw new IOException( "Unable to open the connection is is is ALREADY OPEN; connection status is " + getConnectionStatus() + "." );
		}
		try {
			if ( aOutputStream instanceof ObjectOutputStream ) {
				// -------------------------------------------------------------
				// Hack to enable single threaded pipe streams:
				// -------------------------------------------------------------
				_objectOutputStream = (ObjectOutputStream) aOutputStream;
			}
			else {
				if ( !( aOutputStream instanceof BufferedOutputStream ) ) {
					_objectOutputStream = new ObjectOutputStream( new BufferedOutputStream( aOutputStream ) );
				}
				else {
					_objectOutputStream = new ObjectOutputStream( aOutputStream );
				}
			}
			_objectOutputStream.flush();
		}
		catch ( IOException aException ) {
			throw new IOException( "Unable to open the I/O stream receiver as of a causing exception.", aException );
		}
		// setConnectionStatus( ConnectionStatus.OPENED );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////
}
