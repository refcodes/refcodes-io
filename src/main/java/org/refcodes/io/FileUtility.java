// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.refcodes.data.Delimiter;
import org.refcodes.data.Encoding;
import org.refcodes.data.FilenameExtension;
import org.refcodes.data.Scheme;
import org.refcodes.runtime.Host;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;
import org.refcodes.textual.RandomTextGenerartor;
import org.refcodes.textual.RandomTextMode;

/**
 * The {@link FileUtility} provides Various file related utility functionality.
 */
public final class FileUtility {

	private static final Random RND = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private FileUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines equality of the provided files by comparing their content
	 * (payload).
	 *
	 * @param aFileA The one {@link File} to be compared with the other
	 *        {@link File}.
	 * @param aFileB The other {@link File} to be compared with the one
	 *        {@link File}.
	 * 
	 * @return True in case of equality, else false.
	 * 
	 * @throws FileNotFoundException thrown in case a {@link File} was not
	 *         found.
	 * @throws IOException thrown in case there were I/O related problems.
	 */
	public static boolean isEqual( File aFileA, File aFileB ) throws IOException {
		if ( aFileA.length() != aFileB.length() ) {
			return false;
		}
		try ( InputStream theExpectedInputStream = new BufferedInputStream( new FileInputStream( aFileA ) ); InputStream theCompareInputStream = new BufferedInputStream( new FileInputStream( aFileB ) ) ) {
			int expected;
			int compare;
			while ( ( expected = theExpectedInputStream.read() ) != -1 ) {
				compare = theCompareInputStream.read();
				if ( expected != compare ) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Copies a file residing in a nested JAR to the given destination folder.
	 * The provided folder represents the base folder, actually an unambiguous
	 * folder layout is created within that base folder to prevent side effects
	 * with files of the same name residing in different nested JAR archives.
	 * 
	 * @param aJarUrl The URL which points into a (nested) JAR's resource.
	 * @param aToDir The base directory into which the (nested) JAR's resource
	 *        will be extracted.
	 * 
	 * @return The file URL (protocol "file:") for the extracted resource
	 *         addressed in the (nested) JAR or null in case we do not have a
	 *         JAR URL. In case we already have a file URL then the URL is
	 *         returned untouched ignoring any passed destination folder (the
	 *         to-dir argument).
	 * 
	 * @throws IOException in case processing the extracted file caused
	 *         problems, e.g. the target folder is not a directory, it is not
	 *         writable, the JAR archive caused problems (currpted) and so on.
	 * 
	 * @see "https://docs.jboss.org/jbossas/javadoc/4.0.2/org/jboss/util/file/JarUtils.java.html"
	 */
	public static URL createNestedJarFileUrl( URL aJarUrl, File aToDir ) throws IOException {
		if ( aJarUrl.getProtocol().equals( Scheme.FILE.getName() ) ) {
			return aJarUrl;
		}
		if ( !aJarUrl.getProtocol().equals( Scheme.JAR.getName() ) ) {
			return null;
		}

		// Determine target path:
		final String theJarPath = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( FileUtility.toJarHierarchy( aJarUrl ) ).withDelimiter( Delimiter.SYSTEM_FILE_PATH.getChar() ).toRecord();
		final File theJarDir = new File( aToDir, theJarPath );
		if ( !theJarDir.exists() && !theJarDir.mkdirs() ) {
			throw new IOException( "Failed to create contents directory for archive, path=" + theJarDir.getAbsolutePath() );
		}

		// Process JAR:
		final JarURLConnection theJarConnection = (JarURLConnection) aJarUrl.openConnection();
		final String theEntyName = theJarConnection.getEntryName();
		final File theEntryFile = new File( theJarDir, theEntyName );
		// Do we address a folder?
		if ( theEntyName.endsWith( "" + Delimiter.PATH ) ) {
			theEntryFile.mkdirs();
		}
		// Do we address a folder's entry?
		else {
			final File theJarParentDir = theEntryFile.getParentFile();
			if ( !theJarParentDir.exists() && !theJarParentDir.mkdirs() ) {
				throw new IOException( "Failed to create parent directory for archive, path=" + theJarParentDir.getAbsolutePath() );
			}
			try ( InputStream theJarInputStream = theJarConnection.getInputStream(); BufferedOutputStream theOutputStream = new BufferedOutputStream( new FileOutputStream( theEntryFile ) ) ) {
				final byte[] theBuffer = new byte[4096];
				int eRead;
				while ( ( eRead = theJarInputStream.read( theBuffer ) ) > 0 ) {
					theOutputStream.write( theBuffer, 0, eRead );
				}

			}
		}
		return theEntryFile.toURI().toURL();
	}

	/**
	 * Creates a temporary {@link File} with random content as of the
	 * {@link RandomTextMode#ASCII} of the given size. The temporary
	 * {@link File} is deleted upon exiting the JVM.
	 * 
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * 
	 * @return The temporary {@link File} of the given random content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createRandomTempFile( long aBytes ) throws IOException {
		return createRandomTempFile( toTempBaseFileName(), aBytes, RandomTextMode.ASCII, true );
	}

	/**
	 * Creates a temporary {@link File} with random content as of the
	 * {@link RandomTextMode} of the given size.
	 * 
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param isDeleteOnExit Delete the file upon exit of the JVM.
	 * 
	 * @return The temporary {@link File} of the given random content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createRandomTempFile( long aBytes, boolean isDeleteOnExit ) throws IOException {
		return createRandomTempFile( toTempBaseFileName(), aBytes, RandomTextMode.ASCII, isDeleteOnExit );
	}

	/**
	 * Creates a temporary {@link File} with random content as of the
	 * {@link RandomTextMode} of the given size. The temporary {@link File} is
	 * deleted upon exiting the JVM.
	 * 
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param aRandomTextMode The {@link RandomTextMode} to use when creating
	 *        the {@link File}.
	 * 
	 * @return The temporary {@link File} of the given random content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createRandomTempFile( long aBytes, RandomTextMode aRandomTextMode ) throws IOException {
		return createRandomTempFile( toTempBaseFileName(), aBytes, aRandomTextMode, true );
	}

	/**
	 * Creates a temporary {@link File} with random content as of the
	 * {@link RandomTextMode} of the given size.
	 * 
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param aRandomTextMode The {@link RandomTextMode} to use when creating
	 *        the {@link File}.
	 * @param isDeleteOnExit Delete the file upon exit of the JVM.
	 * 
	 * @return The temporary {@link File} of the given random content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createRandomTempFile( long aBytes, RandomTextMode aRandomTextMode, boolean isDeleteOnExit ) throws IOException {
		return createRandomTempFile( toTempBaseFileName(), aBytes, aRandomTextMode, isDeleteOnExit );
	}

	/**
	 * Creates a temporary {@link File} with random content as of the
	 * {@link RandomTextMode#ASCII} of the given size. The temporary
	 * {@link File} is deleted upon exiting the JVM.
	 * 
	 * @param aBaseFileName The base file name to use.
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * 
	 * @return The temporary {@link File} of the given random content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createRandomTempFile( String aBaseFileName, long aBytes ) throws IOException {
		return createRandomTempFile( aBaseFileName, aBytes, RandomTextMode.ASCII, true );
	}

	/**
	 * Creates a temporary {@link File} with random content as of the
	 * {@link RandomTextMode} of the given size.
	 * 
	 * @param aBaseFileName The base file name to use.
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param isDeleteOnExit Delete the file upon exit of the JVM.
	 * 
	 * @return The temporary {@link File} of the given random content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createRandomTempFile( String aBaseFileName, long aBytes, boolean isDeleteOnExit ) throws IOException {
		return createRandomTempFile( aBaseFileName, aBytes, RandomTextMode.ASCII, isDeleteOnExit );
	}

	/**
	 * Creates a temporary {@link File} with random content as of the
	 * {@link RandomTextMode} of the given size. The temporary {@link File} is
	 * deleted upon exiting the JVM.
	 * 
	 * @param aBaseFileName The base file name to use.
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param aRandomTextMode The {@link RandomTextMode} to use when creating
	 *        the {@link File}.
	 * 
	 * @return The temporary {@link File} of the given random content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createRandomTempFile( String aBaseFileName, long aBytes, RandomTextMode aRandomTextMode ) throws IOException {
		return createRandomTempFile( aBaseFileName, aBytes, aRandomTextMode, true );
	}

	/**
	 * Creates a temporary {@link File} with random content as of the
	 * {@link RandomTextMode} of the given size.
	 * 
	 * @param aBaseFileName The base file name to use.
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param aRandomTextMode The {@link RandomTextMode} to use when creating
	 *        the {@link File}.
	 * @param isDeleteOnExit Delete the file upon exit of the JVM.
	 * 
	 * @return The temporary {@link File} of the given random content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createRandomTempFile( String aBaseFileName, long aBytes, RandomTextMode aRandomTextMode, boolean isDeleteOnExit ) throws IOException {
		final int theLength = aRandomTextMode.getCharSet().length;
		final File theTmpFile = new File( Host.getTempDir(), ( aBaseFileName != null ? aBaseFileName : toTempBaseFileName() ) + FilenameExtension.TEMP.getFilenameSuffix() );
		if ( isDeleteOnExit ) {
			theTmpFile.deleteOnExit();
		}
		try ( FileOutputStream theFileOut = new FileOutputStream( theTmpFile ); OutputStream theBufOut = new BufferedOutputStream( theFileOut ) ) {
			for ( long i = 0; i < aBytes; i++ ) {
				theBufOut.write( aRandomTextMode.getCharSet()[RND.nextInt( theLength )] );
			}
		}
		return theTmpFile;
	}

	/**
	 * Creates a temporary {@link File} filled with content of the specified
	 * value with the given size (number of bytes). The temporary {@link File}
	 * is deleted upon exiting the JVM.
	 * 
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param aValue The value with which to fill the file.
	 * 
	 * @return The temporary {@link File} of the given content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createTempFile( long aBytes, byte aValue ) throws IOException {
		return createTempFile( toTempBaseFileName(), aBytes, aValue, true );
	}

	/**
	 * Creates a temporary {@link File} filled with content of the specified
	 * value with the given size (number of bytes). T
	 * 
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param aValue The value with which to fill the file.
	 * @param isDeleteOnExit Delete the file upon exit of the JVM.
	 * 
	 * @return The temporary {@link File} of the given content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createTempFile( long aBytes, byte aValue, boolean isDeleteOnExit ) throws IOException {
		return createTempFile( toTempBaseFileName(), aBytes, aValue, isDeleteOnExit );
	}

	/**
	 * Creates a temporary {@link File} filled with content of the specified
	 * value with the given size (number of bytes). The temporary {@link File}
	 * is deleted upon exiting the JVM.
	 * 
	 * @param aBaseFileName The base file name to use.
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param aValue The value with which to fill the file.
	 * 
	 * @return The temporary {@link File} of the given content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createTempFile( String aBaseFileName, long aBytes, byte aValue ) throws IOException {
		return createTempFile( aBaseFileName, aBytes, aValue, true );
	}

	/**
	 * Creates a temporary {@link File} filled with content of the specified
	 * value with the given size (number of bytes).
	 * 
	 * @param aBaseFileName The base file name to use.
	 * @param aBytes The size in bytes of the {@link File} to be created.
	 * @param aValue The value with which to fill the file.
	 * @param isDeleteOnExit Delete the file upon exit of the JVM.
	 * 
	 * @return The temporary {@link File} of the given content and size.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createTempFile( String aBaseFileName, long aBytes, byte aValue, boolean isDeleteOnExit ) throws IOException {
		final File theTmpFile = new File( Host.getTempDir(), ( aBaseFileName != null ? aBaseFileName : toTempBaseFileName() ) + FilenameExtension.TEMP.getFilenameSuffix() );
		if ( isDeleteOnExit ) {
			theTmpFile.deleteOnExit();
		}
		try ( FileOutputStream theFileOut = new FileOutputStream( theTmpFile ); OutputStream theBufOut = new BufferedOutputStream( theFileOut ) ) {
			for ( long i = 0; i < aBytes; i++ ) {
				theBufOut.write( aValue );
			}
		}
		return theTmpFile;
	}

	/**
	 * Creates an empty temporary {@link File}. The temporary {@link File} is
	 * deleted upon exiting the JVM.
	 * 
	 * @return The temporary {@link File}.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createTempFile() throws IOException {
		return createTempFile( toTempBaseFileName(), true );
	}

	/**
	 * Creates an empty temporary {@link File}.
	 * 
	 * @param isDeleteOnExit Delete the file upon exit of the JVM.
	 * 
	 * @return The temporary {@link File}.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createTempFile( boolean isDeleteOnExit ) throws IOException {
		return createTempFile( toTempBaseFileName(), isDeleteOnExit );
	}

	/**
	 * Creates an empty temporary {@link File}. The temporary {@link File} is
	 * deleted upon exiting the JVM.
	 * 
	 * @param aBaseFileName The base file name to use.
	 * 
	 * @return The temporary {@link File}.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createTempFile( String aBaseFileName ) throws IOException {
		return createTempFile( aBaseFileName, true );
	}

	/**
	 * Creates an empty temporary {@link File}.
	 * 
	 * @param aBaseFileName The base file name to use.
	 * @param isDeleteOnExit Delete the file upon exit of the JVM.
	 * 
	 * @return The temporary {@link File}.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         creating the file.
	 */
	public static File createTempFile( String aBaseFileName, boolean isDeleteOnExit ) throws IOException {
		final File theTmpFile = new File( Host.getTempDir(), ( aBaseFileName != null ? aBaseFileName : toTempBaseFileName() ) + FilenameExtension.TEMP.getFilenameSuffix() );
		if ( isDeleteOnExit ) {
			theTmpFile.deleteOnExit();
		}
		return theTmpFile;
	}

	/**
	 * Opens a temporary (existing) {@link File} which has been created via
	 * {@link #createTempFile(String)},
	 * {@link #createTempFile(String, boolean)},
	 * {@link #createTempFile(String, long, byte)} or
	 * {@link #createTempFile(String, long, byte, boolean)} (make sure to use
	 * the same base filename when opening as has been used when creating).
	 * 
	 * @param aBaseFileName The base file name of the file to use.
	 * 
	 * @return The according {@link File}.
	 * 
	 * @throws IOException thrown in case there were I/O related problems
	 *         opening the file.
	 */
	public static File openTempFile( String aBaseFileName ) throws IOException {
		final File theTmpFile = new File( Host.getTempDir(), aBaseFileName + FilenameExtension.TEMP.getFilenameSuffix() );
		return theTmpFile;
	}

	/**
	 * Determines whether an according destination file already exists for the
	 * file residing in a nested JAR. The provided folder represents the base
	 * folder; actually an unambiguous folder layout as of
	 * {@link #toJarHierarchy(URL)} is assumed within that base folder (as
	 * created by the {@link #createNestedJarFileUrl(URL, File)}) for preventing
	 * side effects with files of the same name residing in different nested JAR
	 * archives.
	 * 
	 * @param aJarUrl The URL which points into a (nested) JAR's resource.
	 * @param aToDir The base directory in which the (nested) JAR's resource is
	 *        being expected.
	 * 
	 * @return The file URL (protocol "file:") for the identified (existing)
	 *         resource addressed in the (nested) JAR or null in case there is
	 *         no such file in the expected folder layout. In case we already
	 *         have a file URL then the URL is returned untouched ignoring any
	 *         passed destination folder (the to-dir argument).
	 * 
	 * @throws IOException Thrown in case no details on the referenced entry can
	 *         be retrieved from the addressed JAR archive.
	 * 
	 * @see "https://docs.jboss.org/jbossas/javadoc/4.0.2/org/jboss/util/file/JarUtils.java.html"
	 */
	public static URL getNestedJarFileUrl( URL aJarUrl, File aToDir ) throws IOException {
		if ( aJarUrl.getProtocol().equals( Scheme.FILE.getName() ) ) {
			return aJarUrl;
		}
		if ( !aJarUrl.getProtocol().equals( Scheme.JAR.getName() ) ) {
			return null;
		}
		final String theJarPath = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( FileUtility.toJarHierarchy( aJarUrl ) ).withDelimiter( Delimiter.SYSTEM_FILE_PATH.getChar() ).toRecord();
		final File theJarDir = new File( aToDir, theJarPath );
		if ( !theJarDir.exists() ) {
			return null;
		}
		final JarURLConnection theJarConnection;
		theJarConnection = (JarURLConnection) aJarUrl.openConnection();
		final String theEntyName = theJarConnection.getEntryName();
		final File theEntryFile = new File( theJarDir, theEntyName );
		if ( !theEntryFile.exists() ) {
			return null;
		}
		return theEntryFile.toURI().toURL();
	}

	/**
	 * Provides an {@link InputStream} for a resource found at the given path
	 * relative to the given class file (which might be inside a Java archive
	 * such as a JAR file or a WAR file).
	 * 
	 * @param aClass The class relative to which to look for the resource.
	 * @param aPath The path which to use relative to the given class.
	 * 
	 * @return The {@link InputStream} for the requested resource.
	 */
	public static InputStream getResourceAsStream( Class<?> aClass, String aPath ) {
		InputStream theInputStream = aClass.getResourceAsStream( aPath );
		if ( theInputStream == null ) {
			theInputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream( aPath );
		}
		if ( theInputStream == null ) {
			theInputStream = aClass.getClassLoader().getResourceAsStream( aPath );
		}
		return theInputStream;
	}

	/**
	 * Takes an URL pointing into a (nested) JAR resources and returns a list of
	 * JAR archive names (including the ".jar" suffix) in the order of their
	 * nesting, the first JAR archive being the outermost (parent) archive and
	 * the last JAR archive being the innermost archive. A JAR "path" of an URL
	 * might look as follows:
	 * "jar:file:/home/steiner/Workspaces/com.fightclub/fightclub-app/target/fightclub-app-0.0.1-SNAPSHOT.jar!/webapp/home.xhtml"
	 * "jar:file:/home/steiner/Workspaces/com.fightclub/fightclub-app/target/fightclub-app-0.0.1-SNAPSHOT.jar!/lib/fightclub-adapter-web-0.0.1-SNAPSHOT.jar!/webapp/home.xhtml"
	 * 
	 * @param aJarUrl The URL for which to get the JAR file hierarchy array.
	 * 
	 * @return The array with the JAR archive hierarchy or null if not being a
	 *         JAR URL.
	 */
	public static String[] toJarHierarchy( URL aJarUrl ) {
		if ( !aJarUrl.getProtocol().equals( Scheme.JAR.getName() ) ) {
			return null;
		}
		final List<String> theList = new ArrayList<>();
		try {
			String theJarPath = URLDecoder.decode( aJarUrl.getFile(), Encoding.UTF_8.getCode() );
			String eJarFile;
			int i = Scheme.JAR.firstMarkerIndex( theJarPath );
			while ( i != -1 ) {
				eJarFile = theJarPath.substring( 0, i );
				final int j = eJarFile.lastIndexOf( '/' );
				if ( j != -1 ) {
					eJarFile = eJarFile.substring( j + 1 );
				}
				theList.add( eJarFile );
				theJarPath = theJarPath.substring( i );
				i = Scheme.JAR.firstMarkerIndex( theJarPath );
			}

			return theList.toArray( new String[theList.size()] );
		}
		catch ( UnsupportedEncodingException e ) {
			return null;
		}
	}

	/**
	 * Convenience method testing whether the given JAR file resource already
	 * exists in the expected folder layout .Returns its URL in case it already
	 * exists else it is being created and then the URL is returned.
	 *
	 * @param aJarUrl The URL which points into a (nested) JAR's resource.
	 * @param aToDir The base directory in which the (nested) JAR's resource is
	 *        being expected (created).
	 * 
	 * @return The parrent's JAR file URL or null if the application does not
	 *         seem to reside in a JAR. The file URL (protocol "file:") for the
	 *         existing (created) resource addressed in the (nested) JAR.
	 * 
	 * @throws IOException Thrown in case no details on the referenced entry can
	 *         be retrieved from the addressed JAR archive.
	 * 
	 * @see #getNestedJarFileUrl(URL, File)
	 * @see #createNestedJarFileUrl(URL, File)
	 */
	public static URL toNestedJarFileUrl( URL aJarUrl, File aToDir ) throws IOException {
		final URL theUrl = getNestedJarFileUrl( aJarUrl, aToDir );
		return theUrl != null ? theUrl : createNestedJarFileUrl( aJarUrl, aToDir );
	}

	/**
	 * Determines the parent JAR file's URL for your running application (not
	 * including the "!" which is required to address a file within that JAR).
	 * 
	 * @return The parrent's JAR file URL or null if the application does not
	 *         seem to reside in a JAR.
	 */
	public static URL toParentJarUrl() {
		final URL theUrl = FileUtility.class.getProtectionDomain().getCodeSource().getLocation();
		if ( !theUrl.getProtocol().equals( Scheme.JAR.getName() ) ) {
			return null;
		}
		try {
			String theJarPath = URLDecoder.decode( theUrl.getFile(), Encoding.UTF_8.getCode() );
			theJarPath = Scheme.JAR.toUrl( theJarPath );
			return new URL( theJarPath );
		}
		catch ( UnsupportedEncodingException | MalformedURLException e ) {
			return null;
		}
	}

	/**
	 * Generates a base name for a temporary file (excluding a file suffix)
	 * consisting if the current time in milliseconds and a portion of random
	 * characters to avoid name clashes:
	 * "temp-012345678901234567890123456789-abcdefgh".
	 * 
	 * @return A temp file name.
	 */
	public static String toTempBaseFileName() {
		return "temp-" + System.currentTimeMillis() + "-" + RandomTextGenerartor.asString( 8, RandomTextMode.LOWER_CASE_ALPHANUMERIC );
	}

	/**
	 * Generates a file name for a temporary file consisting if the current time
	 * in milliseconds and a portion of random characters to avoid name clashes:
	 * "temp-012345678901234567890123456789-abcdefgh.tmp".
	 * 
	 * @return A temp file name.
	 */
	public static String toTempFileName() {
		return toTempBaseFileName() + FilenameExtension.TEMP.getFilenameSuffix();
	}
}
