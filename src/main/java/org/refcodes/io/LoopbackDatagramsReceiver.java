// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.Serializable;

/**
 * A {@link LoopbackDatagramsReceiver} is a {@link DatagramReceiver} connected
 * directly within the same JVM with a {@link LoopbackDatagramsTransmitter}; a
 * "loopback" connection is used for establishing the connection which cannot be
 * accessed outside the JVM or the running machine.
 *
 * @param <DATA> The type of the datagram to be operated with.
 */
public class LoopbackDatagramsReceiver<DATA extends Serializable> extends AbstractDatagramsReceiver<DATA> implements ConnectionDatagramsReceiver<DATA, LoopbackDatagramsTransmitter<DATA>> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LoopbackDatagramsTransmitter<DATA> _loopbackSender;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new loopback receiver impl.
	 */
	public LoopbackDatagramsReceiver() {}

	/**
	 * Instantiates a new loopback receiver impl.
	 *
	 * @param aCapacity the capacity
	 */
	public LoopbackDatagramsReceiver( int aCapacity ) {
		super( aCapacity );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable( LoopbackDatagramsTransmitter<DATA> aLoopbackSender ) {
		return super.isOpenable() && aLoopbackSender != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void open( LoopbackDatagramsTransmitter<DATA> aLoopbackSender ) throws IOException {
		if ( isOpened() ) {
			if ( _loopbackSender == aLoopbackSender && _loopbackSender.isOpened() ) {
				return;
			}
			throw new IOException( "Unable to open the connection is is is ALREADY OPEN; connection status is " + getConnectionStatus() + "." );
		}
		super.open();
		_loopbackSender = aLoopbackSender;
		if ( _loopbackSender.isOpenable( this ) ) {
			_loopbackSender.open( this );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pushDatagram( DATA aDatagram ) throws IOException {
		super.pushDatagram( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pushDatagrams( DATA[] aDatagrams ) throws IOException {
		super.pushDatagrams( aDatagrams );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pushDatagrams( DATA[] aDatagrams, int aOffset, int aLength ) throws IOException {
		super.pushDatagrams( aDatagrams, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void close() throws IOException {
		super.close();
		if ( _loopbackSender != null && !_loopbackSender.isClosed() ) {
			_loopbackSender.close();
			_loopbackSender = null;
		}
	}
}
