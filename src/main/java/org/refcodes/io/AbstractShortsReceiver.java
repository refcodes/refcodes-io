// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.EOFException;
import java.io.IOException;

import org.refcodes.component.AbstractConnectableAutomaton;
import org.refcodes.component.ConnectionStatus;

/**
 * The {@link AbstractShortsReceiver} is a base abstract implementation of the
 * {@link ShortsReceiver} interface providing common functionality for concrete
 * real live implementations.
 */
public abstract class AbstractShortsReceiver extends AbstractShortsDestination implements ShortsReceiver {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected ConnectableAutomaton _automaton = new ConnectableAutomaton();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link AbstractShortsReceiver} with a default sized blocking
	 * queue enabling a maximum of {@link #DATAGRAM_QUEUE_SIZE} datagrams.
	 * -------------------------------------------------------------------------
	 * Make sure your code fetches the datagrams quick enough to prevent filling
	 * up of the queue. In case the queue is filled up, adding elements via
	 * {@link #pushDatagram(short)} to the queue is blocked until elements are
	 * taken from the queue via {@link #receiveShort()}.
	 */
	public AbstractShortsReceiver() {}

	/**
	 * Constructs a {@link AbstractShortsReceiver} with a custom sized blocking
	 * queue enabling a maximum of datagrams as specified by the capacity
	 * parameter.
	 * -------------------------------------------------------------------------
	 * Make sure your code fetches the datagrams quick enough to prevent filling
	 * up of the queue. In case the queue is filled up, adding elements via
	 * {@link #pushDatagram(short)} to the queue is blocked until elements are
	 * taken from the queue via {@link #receiveShort()}.
	 * 
	 * @param aCapacity The capacity of the queue holding the received
	 *        datagrams.
	 */
	public AbstractShortsReceiver( int aCapacity ) {
		super( aCapacity );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short receiveShort() throws IOException {
		if ( _datagramQueue.isEmpty() && isClosed() ) {
			throw new EOFException( "Connection is closed and no (more) data is available (connection status is <" + getConnectionStatus() + ">)." );
		}
		if ( _datagramQueue.isEmpty() && !isOpened() ) {
			throw new IOException( "Unable to read datagram  as the connection is NOT OPEN (connection status is " + getConnectionStatus() + ")." );
		}
		return super.receiveShort();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short[] receiveAllShorts() throws IOException {
		if ( _datagramQueue.isEmpty() && isClosed() ) {
			throw new EOFException( "Connection is closed and no (more) data is available (connection status is <" + getConnectionStatus() + ">)." );
		}
		if ( _datagramQueue.isEmpty() && !isOpened() ) {
			throw new IOException( "Unable to read datagram  as the connection is NOT OPEN (connection status is " + getConnectionStatus() + ")." );
		}
		return super.receiveAllShorts();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short[] receiveShorts( int aLength ) throws IOException {
		if ( _datagramQueue.isEmpty() && isClosed() ) {
			throw new EOFException( "Connection is closed and no (more) data is available (connection status is <" + getConnectionStatus() + ">)." );
		}
		if ( _datagramQueue.isEmpty() && !isOpened() ) {
			throw new IOException( "Unable to read datagram  as the connection is NOT OPEN (connection status is " + getConnectionStatus() + ")." );
		}
		return super.receiveShorts( aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		// Enable the caller to get all elements from the queue:
		// if ( _datagramQueue.isEmpty() && !isOpened() ) { throw new
		// IOException( "Unable to read datagram as the connection is NOT
		// OPEN; connection status is " + getConnectionStatus() + "." ); }
		return _datagramQueue.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( isOpened() ) {
			_automaton.close();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		return _automaton.isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosable() {
		return _automaton.isClosable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosed() {
		return _automaton.isClosed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _automaton.getConnectionStatus();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Open.
	 *
	 * @throws IOException the open exception
	 */
	protected void open() throws IOException {
		_automaton.open();
	}

	/**
	 * Sets the connection status.
	 *
	 * @param aConnectionStatus the new connection status
	 */
	protected void setConnectionStatus( ConnectionStatus aConnectionStatus ) {
		_automaton.setConnectionStatus( aConnectionStatus );
	}

	/**
	 * Checks if is openable.
	 *
	 * @return true, if is openable
	 */
	protected boolean isOpenable() {
		return _automaton.isOpenable();
	}

	///////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Class ConnectableAutomaton.
	 */
	protected static class ConnectableAutomaton extends AbstractConnectableAutomaton {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isOpenable() {
			return super.isOpenable();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void open() throws IOException {
			super.open();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void setConnectionStatus( ConnectionStatus aConnectionStatus ) {
			super.setConnectionStatus( aConnectionStatus );
		}
	};
}
