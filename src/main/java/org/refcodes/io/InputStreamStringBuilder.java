// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.refcodes.data.Encoding;
import org.refcodes.exception.RuntimeIOException;
import org.refcodes.mixin.EncodingAccessor.EncodingBuilder;
import org.refcodes.mixin.EncodingAccessor.EncodingProperty;
import org.refcodes.mixin.InputStreamAccessor.InputStreamBuilder;
import org.refcodes.mixin.InputStreamAccessor.InputStreamProperty;

/**
 * The {@link InputStreamStringBuilder} constructs {@link String} instances from
 * {@link InputStream} instances. Either use
 * {@link #setInputStream(InputStream)} ({@link #withInputStream(InputStream)})
 * followed by a {@link Object#toString()} method (not thread safe) or directly
 * call {@link #toString(InputStream)} (thread safe). You may specify an
 * encoding to be used such as UTF-8 by either setting the encoding attribute
 * with {@link #setEncoding(Object)} ({@link #withEncoding(String)}) or by
 * passing the encoding to the conversion method as of {@link #toString(String)}
 * which overrules the encoding attribute.
 */
public class InputStreamStringBuilder implements InputStreamProperty, InputStreamBuilder<InputStreamStringBuilder>, EncodingProperty<String>, EncodingBuilder<String, InputStreamStringBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private InputStream _inputStream = null;
	private String _lines = null;
	private String _encoding = Encoding.UTF_8.getCode();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStreamStringBuilder withEncoding( String aEncoding ) {
		setEncoding( aEncoding );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStreamStringBuilder withInputStream( InputStream aInputStream ) {
		setInputStream( aInputStream );
		return this;
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @param aInputStream The {@link InputStream} which to convert into a
	 *        {@link String}.
	 * 
	 * @return The according resulting {@link String}
	 * 
	 * @throws IOException thrown in case accessing the {@link InputStream}
	 *         caused faults.
	 */
	public String toString( InputStream aInputStream ) throws IOException {
		return toString( aInputStream, getEncoding() );
	}

	/**
	 * The {@link String}s being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @param aInputStream The {@link InputStream} which to convert into a
	 *        {@link String} array.
	 * 
	 * @return The according resulting {@link String} array
	 * 
	 * @throws IOException thrown in case accessing the {@link InputStream}
	 *         caused faults.
	 */
	public String[] toStrings( InputStream aInputStream ) throws IOException {
		return toString( aInputStream, getEncoding() ).split( "\\r\\n|\\n|\\r" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEncoding() {
		return _encoding;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEncoding( String aEncoding ) {
		_encoding = aEncoding;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream getInputStream() {
		return _inputStream;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setInputStream( InputStream aInputStream ) {
		_inputStream = aInputStream;
		_lines = null;
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @param aInputStream The {@link InputStream} which to convert into a
	 *        {@link String}.
	 * @param aEncoding The text encoding to be used.
	 * 
	 * @return The according resulting {@link String}
	 * 
	 * @throws IOException thrown in case accessing the {@link InputStream}
	 *         caused faults.
	 */
	public String toString( InputStream aInputStream, String aEncoding ) throws IOException {
		if ( aInputStream == null ) {
			return null;
		}
		try ( Scanner theScanner = new Scanner( aInputStream, aEncoding ) ) {
			theScanner.useDelimiter( "\\A" );
			return theScanner.hasNext() ? theScanner.next() : "";
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		try {
			return toString( _encoding );
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}

	/**
	 * The {@link String}s being build by the builder upon the settings of the
	 * attributes.
	 *
	 * @param aEncoding The text encoding to be used.
	 * 
	 * @return The according resulting {@link String} array
	 * 
	 * @throws IOException thrown in case accessing the {@link InputStream}
	 *         caused faults.
	 */
	public String toString( String aEncoding ) throws IOException {
		if ( _lines == null ) {
			_lines = toString( _inputStream, aEncoding );
		}
		return _lines;
	}

	/**
	 * The {@link String}s being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @return The according resulting {@link String} array
	 * 
	 * @throws IOException thrown in case accessing the {@link InputStream}
	 *         caused faults.
	 */
	public String[] toStrings() throws IOException {
		return toStrings( getEncoding() );
	}

	/**
	 * The {@link String}s being build by the builder upon the settings of the
	 * attributes.
	 *
	 * @param aEncoding The text encoding to be used.
	 * 
	 * @return The according resulting {@link String} array
	 * 
	 * @throws IOException thrown in case accessing the {@link InputStream}
	 *         caused faults.
	 */
	public String[] toStrings( String aEncoding ) throws IOException {
		return toString( aEncoding ).split( "\\r\\n|\\n|\\r" );
	}

	/**
	 * The {@link String}s being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @param aInputStream The {@link InputStream} which to convert into a
	 *        {@link String} array.
	 * @param aEncoding The text encoding to be used.
	 * 
	 * @return The according resulting {@link String} array
	 * 
	 * @throws IOException thrown in case accessing the {@link InputStream}
	 *         caused faults.
	 */
	public String[] toStrings( InputStream aInputStream, String aEncoding ) throws IOException {
		return toString( aInputStream, aEncoding ).split( "\\r\\n|\\n|\\r" );
	}
}
