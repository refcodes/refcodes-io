// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@link BytesDestination} is used to receive byte blocks (arrays) in a
 * unified way. The {@link #receiveAllBytes()} method provides the next
 * available short block from the counterpart {@link DatagramsTransmitter} or
 * {@link DatagramTransmitter}; in case there is none available, then this
 * method halts until one is available.
 */
@FunctionalInterface
public interface BytesDestination extends ByteDestination {

	/**
	 * {@inheritDoc}
	 */
	@Override
	byte receiveByte() throws IOException;

	/**
	 * Reads (receives) the all currently available data.
	 * 
	 * @return The according data.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	default byte[] receiveAllBytes() throws IOException {
		return receiveBytes( -1 );
	}

	/**
	 * Similar to {@link #receiveAllBytes()} though at maximum the amount of
	 * data as of the provided length returned.
	 * 
	 * @param aLength The block-size which is not to exceeded by the returned
	 *        data. A value of -1 specifies to retrieve all available datagrams
	 *        (same behavior as method {@link #receiveAllBytes()}.
	 * 
	 * @return The according data.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	default byte[] receiveBytes( int aLength ) throws IOException {
		final List<Byte> theDatagrams = new ArrayList<>();
		try {
			if ( aLength > 0 ) {
				for ( int i = 0; i < aLength; i++ ) {
					theDatagrams.add( receiveByte() );
				}
			}
			else {
				while ( true ) {
					theDatagrams.add( receiveByte() );
				}
			}
		}
		catch ( IOException e ) {
			if ( theDatagrams.isEmpty() ) {
				throw e;
			}
		}
		// return toPrimitiveType( theDatagrams.toArray( new Byte[theDatagrams.size()] ) );
		final Byte[] aBytes = theDatagrams.toArray( new Byte[theDatagrams.size()] );
		if ( aBytes == null ) {
			return null;
		}
		final byte[] thePrimitives = new byte[aBytes.length];
		for ( int i = 0; i < aBytes.length; i++ ) {
			thePrimitives[i] = aBytes[i].byteValue();
		}
		return thePrimitives;
	}

	/**
	 * Receives a byte array with the number of bytes specified inserted at the
	 * given offset. This method blocks till a byte is available.
	 *
	 * @param aBuffer The byte array where to store the bytes at.
	 * @param aOffset The offset where to start storing the received bytes.
	 * @param aLength The number of bytes to receive.
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a timeout) while
	 *         receiving.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	default void receiveBytes( byte[] aBuffer, int aOffset, int aLength ) throws IOException {
		for ( int i = aOffset; i < aOffset + aLength; i++ ) {
			aBuffer[i] = receiveByte();
		}
	}
}
