// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * A {@link BoundedInputStream} decorates an {@link InputStream} and restricts
 * reading bytes from the {@link InputStream} decoratee up to a given total
 * number of bytes (bounds). In case the total number of bytes readable has been
 * exceeded then an according {@link IOException} is thrown.
 */
public class BoundedInputStream extends InputStream {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _totalBytes;
	private int _bytesRead = 0;
	private final InputStream _inputStream;
	private int _mark = -1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link BoundedInputStream} instances by decorating the given
	 * {@link InputStream} with the constraint of the given total number of
	 * bytes (bounds) which can be read.
	 * 
	 * @param aInputStream The {@link InputStream} to be decorated.
	 * @param aTotalBytes The total number of bytes (bounds) available for
	 *        reading.
	 */
	public BoundedInputStream( InputStream aInputStream, int aTotalBytes ) {
		_inputStream = aInputStream;
		_totalBytes = aTotalBytes;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IOException if an I/O error occurs or the total number of bytes
	 *         readable would be exceeded (has already been consumed).
	 */
	@Override
	public int read() throws IOException {
		if ( _bytesRead >= _totalBytes ) {
			throw new IOException( "The number of bytes <" + _bytesRead + "> being read reached the length limit <" + _totalBytes + "> to which the underlying input stream is bounded." );
		}
		final int theResult = _inputStream.read();
		_bytesRead++;
		return theResult;
	}

	/**
	 * The total number of bytes (bounds) which can be read from this
	 * {@link InputStream}.
	 * 
	 * @return The overall number of bytes which can be read.
	 */
	public int getTotalBytes() {
		return _totalBytes;
	}

	/**
	 * Returns the number of bytes already been read by this stream.
	 * 
	 * @return The number of bytes already read.
	 */
	public int getBytesRead() {
		return _bytesRead;
	}

	/**
	 * Returns the total number of bytes to which this {@link InputStream} is
	 * bounded.
	 * 
	 * @return The total number of bytes (bounds).
	 */
	public int getBytesAvaialable() {
		return _totalBytes - _bytesRead;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _inputStream.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_inputStream.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void mark( int readlimit ) {
		_inputStream.mark( readlimit );
		if ( markSupported() ) {
			_mark = _bytesRead;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported() {
		return _inputStream.markSupported();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] b, int off, int len ) throws IOException {
		return _inputStream.read( b, off, len );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] b ) throws IOException {
		return _inputStream.read( b );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] readAllBytes() throws IOException {
		return _inputStream.readAllBytes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int readNBytes( byte[] arg0, int arg1, int arg2 ) throws IOException {
		return _inputStream.readNBytes( arg0, arg1, arg2 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void reset() throws IOException {
		_inputStream.reset();
		if ( markSupported() && _mark != -1 ) {
			_bytesRead = _mark;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip( long n ) throws IOException {
		return _inputStream.skip( n );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long transferTo( OutputStream out ) throws IOException {
		return _inputStream.transferTo( out );
	}
}
