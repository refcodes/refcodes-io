// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.Serializable;

/**
 * A {@link LoopbackDatagramsTransceiver} is a {@link DatagramTransceiver}
 * connected directly within the same JVM with another
 * {@link LoopbackDatagramsTransceiver}; a "loopback" connection is used for
 * establishing the connection which cannot be accessed outside the JVM or the
 * running machine.
 *
 * @param <DATA> The type of the datagram to be operated with.
 */
public class LoopbackDatagramsTransceiver<DATA extends Serializable> extends AbstractDatagramsReceiver<DATA> implements ConnectionDatagramsTransceiver<DATA, LoopbackDatagramsTransceiver<DATA>> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LoopbackDatagramsTransceiver<DATA> _loopbackTransceiver = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new loopback transceiver impl.
	 */
	public LoopbackDatagramsTransceiver() {}

	/**
	 * Instantiates a new loopback transceiver impl.
	 *
	 * @param aCapacity the capacity
	 */
	public LoopbackDatagramsTransceiver( int aCapacity ) {
		super( aCapacity );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable( LoopbackDatagramsTransceiver<DATA> aLoopbackTransceiver ) {
		return !isOpened() && aLoopbackTransceiver != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void open( LoopbackDatagramsTransceiver<DATA> aLoopbackTransceiver ) throws IOException {
		if ( isOpened() ) {
			if ( _loopbackTransceiver == aLoopbackTransceiver && _loopbackTransceiver.isOpened() ) {
				return;
			}
			throw new IOException( "Unable to open the connection is is is ALREADY OPEN; connection status is " + getConnectionStatus() + "." );
		}
		super.open();
		_loopbackTransceiver = aLoopbackTransceiver;
		if ( _loopbackTransceiver.isOpenable( this ) ) {
			_loopbackTransceiver.open( this );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA aDatagram ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Unable to write datagram <" + aDatagram.getClass().getName() + "> as the connection is NOT OPEN; connection status is " + getConnectionStatus() + "." );
		}
		_loopbackTransceiver.pushDatagram( aDatagram );
	}

	/**
	 * Pushes a datagram into the {@link LoopbackDatagramsReceiver}. Them
	 * datagrams can be retrieved via {@link #receive()}: use
	 * {@link #available()} to test beforehand whether there is a datagram
	 * available.
	 *
	 * @param aDatagram The datagram to be pushed into the
	 *        {@link LoopbackDatagramsReceiver}; to be retrieved with the
	 *        {@link #receive()} method.
	 * 
	 * @throws IOException the open exception
	 */
	@Override
	public void pushDatagram( DATA aDatagram ) throws IOException {
		super.pushDatagram( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_loopbackTransceiver.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void close() throws IOException {
		super.close();
		if ( _loopbackTransceiver != null && !_loopbackTransceiver.isClosed() ) {
			_loopbackTransceiver.close();
			_loopbackTransceiver = null;
		}
	}
}
