// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.refcodes.struct.ByteArrayAccessor;

/**
 * The Class ByteArraySource.
 */
public class ByteArraySource implements BytesSource, ByteArrayAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final List<Byte> _byteList = new ArrayList<>();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitByte( byte aByte ) throws IOException {
		_byteList.add( aByte );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitBytes( byte[] aBytes, int aOffset, int aLength ) throws IOException {
		for ( int i = aOffset; i < aLength; i++ ) {
			_byteList.add( aBytes[i] );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getBytes() {
		return toPrimitiveType( _byteList.toArray( new Byte[_byteList.size()] ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static byte[] toPrimitiveType( Byte[] aBytes ) {
		if ( aBytes == null ) {
			return null;
		}
		final byte[] thePrimitives = new byte[aBytes.length];
		for ( int i = 0; i < aBytes.length; i++ ) {
			thePrimitives[i] = aBytes[i].byteValue();
		}
		return thePrimitives;
	}
}
