// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * The {@link ReplayInputStream} wraps an {@link InputStream} adding
 * {@link #mark(int)}, {@link #markSupported()} as well as {@link #reset()}
 * functionality, similar to the {@link BufferedInputStream}.
 * 
 * In contrast to the {@link BufferedInputStream} only the bytes actually read
 * (by an application) are buffered, preventing it to hang as the
 * {@link BufferedInputStream} would do when it tries to load a buffer of
 * predefined size from (e.g. a serial TTY/COM port) {@link InputStream} which
 * blocks till the requested data is actually available, making the whole
 * application to "hang" (even though just one byte was to be read).
 * 
 * The {@link ReplayInputStream} always uses its own {@link #mark(int)},
 * {@link #markSupported()} as well as {@link #reset()} implementations even if
 * the wrapped {@link InputStream} (such as the {@link BufferedInputStream}
 * supports {@link #mark(int)} and {@link #reset()}!
 */
public class ReplayInputStream extends InputStream {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final InputStream _inputStream;
	private int[] _buffer = null;
	private int _readIndex = -1;
	private int _writeIndex = -1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link InputStream} with {@link #mark(int)},
	 * {@link #markSupported()} as well as {@link #reset()} functionality
	 * 
	 * @param aInputStream The {@link InputStream} to be decorated accordingly.
	 */
	public ReplayInputStream( InputStream aInputStream ) {
		_inputStream = aInputStream;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException {
		if ( _readIndex >= 0 && _readIndex < _buffer.length && _writeIndex > _readIndex ) {
			return _buffer[_readIndex++];
		}
		// ---------------------------------------------------------------------
		// Read byte array and determine the End-of-Stream (-1) upon the number
		// of bytes actually read as some InputStream implementations return signed
		// bytes (including -1) instead of unsigned integers when invoking the read()
		// operation.
		// ---------------------------------------------------------------------
		final byte[] theValue = new byte[1];
		if ( _inputStream.read( theValue ) <= 0 ) {
			return -1;
		}
		// ---------------------------------------------------------------------
		if ( _writeIndex >= 0 ) {
			if ( _writeIndex < _buffer.length ) {
				_buffer[_writeIndex] = theValue[0];
			}
			_writeIndex++;
			_readIndex++;
		}
		return Byte.toUnsignedInt( theValue[0] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _inputStream.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		super.close();
		_inputStream.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void mark( int readlimit ) {
		if ( _buffer == null || _buffer.length != readlimit ) {
			_buffer = new int[readlimit];
		}
		_readIndex = 0;
		_writeIndex = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void reset() throws IOException {
		if ( _writeIndex == -1 || _writeIndex > _buffer.length ) {
			throw new IOException( "Resetting to invalid mark from index <" + _writeIndex + "> outside the scope of the read limit <" + ( _buffer != null ? _buffer.length : 0 ) + ">" );
		}
		_readIndex = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported() {
		return true;
	}
}
