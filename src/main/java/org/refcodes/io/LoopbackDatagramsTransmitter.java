// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.Serializable;

/**
 * A {@link LoopbackDatagramsTransmitter} is a {@link DatagramTransmitter}
 * connected directly within the same JVM with a
 * {@link LoopbackDatagramsReceiver}; a "loopback" connection is used for
 * establishing the connection which cannot be accessed outside the JVM or the
 * running machine.
 *
 * @param <DATA> The type of the datagram to be operated with.
 */
public class LoopbackDatagramsTransmitter<DATA extends Serializable> extends AbstractDatagramsTransmitter<DATA> implements ConnectionDatagramsTransmitter<DATA, LoopbackDatagramsReceiver<DATA>> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LoopbackDatagramsReceiver<DATA> _loopbackReceiver;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable( LoopbackDatagramsReceiver<DATA> aLoopbackReceiver ) {
		return !isOpened() && aLoopbackReceiver != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void open( LoopbackDatagramsReceiver<DATA> aLoopbackReceiver ) throws IOException {
		if ( isOpened() ) {
			if ( _loopbackReceiver == aLoopbackReceiver && _loopbackReceiver.isOpened() ) {
				return;
			}
			throw new IOException( "Unable to open the connection is is is ALREADY OPEN; connection status is " + getConnectionStatus() + "." );
		}
		super.open();
		_loopbackReceiver = aLoopbackReceiver;
		if ( _loopbackReceiver.isOpenable( this ) ) {
			_loopbackReceiver.open( this );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void transmit( DATA aDatagram ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Unable to write datagram <" + aDatagram.getClass().getName() + "> as the connection is NOT OPEN; connection status is " + getConnectionStatus() + "." );
		}
		_loopbackReceiver.pushDatagram( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA[] aDatagrams, int aOffset, int aLength ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Unable to write datagram <" + aDatagrams + "> as the connection is NOT OPEN; connection status is " + getConnectionStatus() + "." );
		}
		_loopbackReceiver.pushDatagrams( aDatagrams, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA[] aDatagrams ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Unable to write datagram <" + aDatagrams + "> as the connection is NOT OPEN; connection status is " + getConnectionStatus() + "." );
		}
		_loopbackReceiver.pushDatagrams( aDatagrams );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		// Nothing to do as datagrams are pushed directly to loopback receiver
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		super.close();
		if ( _loopbackReceiver != null && !_loopbackReceiver.isClosed() ) {
			_loopbackReceiver.close();
			_loopbackReceiver = null;
		}
	}
}
