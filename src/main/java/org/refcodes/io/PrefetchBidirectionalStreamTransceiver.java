// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.concurrent.ExecutorService;

import org.refcodes.component.ConnectionStatus;

/**
 * The Class PrefetchBidirectionalStreamTransceiver.
 *
 * @author steiner
 * 
 * @param <DATA> the generic type
 */
public class PrefetchBidirectionalStreamTransceiver<DATA extends Serializable> implements DatagramsTransceiver<DATA> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final BidirectionalStreamConnectionTransceiver<DATA> _transceiver;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new prefetch bidirectional stream transceiver impl.
	 *
	 * @param aInputStream the input stream
	 * @param aOutputStream the output stream
	 * 
	 * @throws IOException the open exception
	 */
	public PrefetchBidirectionalStreamTransceiver( InputStream aInputStream, OutputStream aOutputStream ) throws IOException {
		_transceiver = new PrefetchBidirectionalStreamConnectionTransceiver<>();
		_transceiver.open( aInputStream, aOutputStream );
	}

	/**
	 * Instantiates a new prefetch bidirectional stream transceiver impl.
	 *
	 * @param aInputStream the input stream
	 * @param aOutputStream the output stream
	 * @param aExecutorService the executor service
	 * 
	 * @throws IOException the open exception
	 */
	public PrefetchBidirectionalStreamTransceiver( InputStream aInputStream, OutputStream aOutputStream, ExecutorService aExecutorService ) throws IOException {
		_transceiver = new PrefetchBidirectionalStreamConnectionTransceiver<>( aExecutorService );
		_transceiver.open( aInputStream, aOutputStream );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosed() {
		return _transceiver.isClosed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		return _transceiver.isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _transceiver.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DATA[] receiveAll() throws IOException {
		return _transceiver.receiveAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _transceiver.getConnectionStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_transceiver.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DATA receive() throws IOException {
		return _transceiver.receive();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA[] aDatagram ) throws IOException {
		_transceiver.transmit( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA[] aDatagram, int aOffset, int aLength ) throws IOException {
		_transceiver.transmit( aDatagram, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA aDatagram ) throws IOException {
		_transceiver.transmit( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_transceiver.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosable() {
		return _transceiver.isClosable();
	}
}
