// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.Serializable;

import org.refcodes.component.Closable;
import org.refcodes.component.Flushable;

/**
 * The {@link TransmitterDecorator} decorates a {@link DatagramsSource} with the
 * additional methods of a {@link DatagramsTransmitter} making it easy to use a
 * {@link DatagramsSource} wherever a {@link DatagramsTransmitter} is expected.
 *
 * @param <DATA> the generic type
 */
public class TransmitterDecorator<DATA extends Serializable> extends AbstractDatagramsTransmitter<DATA> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final DatagramsSource<DATA> _consumer;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new sender decorator.
	 *
	 * @param aConsumer the consumer
	 */
	public TransmitterDecorator( DatagramsSource<DATA> aConsumer ) {
		_consumer = aConsumer;
		try {
			open();
		}
		catch ( IOException ignore ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA aDatagram ) throws IOException {
		_consumer.transmit( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA[] aDatagrams ) throws IOException {
		_consumer.transmit( aDatagrams );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA[] aDatagrams, int aOffset, int aLength ) throws IOException {
		_consumer.transmit( aDatagrams, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		if ( _consumer instanceof Flushable ) {
			( (Flushable) _consumer ).flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( _consumer instanceof Closable ) {
			( (Closable) _consumer ).close();
		}
		super.close();
	}
}
