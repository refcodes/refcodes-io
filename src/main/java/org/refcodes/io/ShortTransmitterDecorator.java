// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;

import org.refcodes.component.Closable;
import org.refcodes.component.Flushable;

/**
 * The {@link ShortTransmitterDecorator} decorates a {@link ShortsSource} with
 * the additional methods of a {@link ShortsTransmitter} making it easy to use a
 * {@link ShortsSource} wherever a {@link ShortsTransmitter} is expected.
 */
public class ShortTransmitterDecorator extends AbstractShortsTransmitter {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final ShortsSource _shortConsumer;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new short sender decorator.
	 *
	 * @param aShortConsumer the short consumer
	 */
	public ShortTransmitterDecorator( ShortsSource aShortConsumer ) {
		_shortConsumer = aShortConsumer;
		try {
			open();
		}
		catch ( IOException ignore ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitShort( short aShort ) throws IOException {
		_shortConsumer.transmitShort( aShort );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitShorts( short[] aShorts ) throws IOException {
		_shortConsumer.transmitShorts( aShorts );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitShorts( short[] aShorts, int aOffset, int aLength ) throws IOException {
		_shortConsumer.transmitShorts( aShorts, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		if ( _shortConsumer instanceof Flushable ) {
			( (Flushable) _shortConsumer ).flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( _shortConsumer instanceof Closable ) {
			( (Closable) _shortConsumer ).close();
		}
		super.close();
	}
}
