// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * The {@link FilterInputStream} wraps an {@link InputStream} adding filtering
 * functionality of bytes to be skipped.
 */
public class FilterInputStream extends InputStream {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final InputStream _inputStream;
	private final byte[] _filterBytes;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link InputStream} filtering functionality.
	 * 
	 * @param aInputStream The {@link InputStream} to be decorated accordingly.
	 * 
	 * @param aFilterBytes The bytes to be skipped (filtered out).
	 */
	public FilterInputStream( InputStream aInputStream, byte... aFilterBytes ) {
		_inputStream = aInputStream;
		_filterBytes = aFilterBytes;
	}

	/**
	 * Decorates the provided {@link InputStream} filtering functionality.
	 * 
	 * @param aInputStream The {@link InputStream} to be decorated accordingly.
	 * 
	 * @param aFilterAsciiChars The ASCII characters to be skipped (filtered
	 *        out).
	 * 
	 * @throws IllegalArgumentException thrown in case a character is not an
	 *         ASCII character.
	 */
	public FilterInputStream( InputStream aInputStream, char... aFilterAsciiChars ) {
		_inputStream = aInputStream;
		_filterBytes = new byte[aFilterAsciiChars.length];
		int eCharValue;
		for ( int i = 0; i < aFilterAsciiChars.length; i++ ) {
			eCharValue = aFilterAsciiChars[i];
			if ( eCharValue > 255 ) {
				throw new IllegalArgumentException( "The character '" + aFilterAsciiChars[i] + "' (value = <" + eCharValue + ">) is greater than <255> and not an ASCII (8-Bit) character!" );
			}
			_filterBytes[i] = (byte) eCharValue;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException {
		int eRead = _inputStream.read();
		while ( eRead != -1 && isFilterByte( eRead ) ) {
			eRead = _inputStream.read();
		}
		return eRead;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _inputStream.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		super.close();
		_inputStream.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void mark( int readlimit ) {
		_inputStream.mark( readlimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void reset() throws IOException {
		_inputStream.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported() {
		return _inputStream.markSupported();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private boolean isFilterByte( int aRead ) {
		for ( byte _filterByte : _filterBytes ) {
			if ( _filterByte == (byte) aRead ) {
				return true;
			}
		}
		return false;
	}
}
