// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;

/**
 * A {@link LoopbackShortsTransceiver} is a {@link DatagramTransceiver}
 * connected directly within the same JVM with another
 * {@link LoopbackShortsTransceiver}; a "loopback" connection is used for
 * establishing the connection which cannot be accessed outside the JVM or the
 * running machine.
 */
public class LoopbackShortsTransceiver extends AbstractShortsReceiver implements ConnectionShortsTransceiver<LoopbackShortsTransceiver> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LoopbackShortsTransceiver _loopbackTransceiver = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new loopback short transceiver impl.
	 */
	public LoopbackShortsTransceiver() {}

	/**
	 * Instantiates a new loopback short transceiver impl.
	 *
	 * @param aCapacity the capacity
	 */
	public LoopbackShortsTransceiver( int aCapacity ) {
		super( aCapacity );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable( LoopbackShortsTransceiver aLoopbackTransceiver ) {
		return !isOpened() && aLoopbackTransceiver != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void open( LoopbackShortsTransceiver aLoopbackTransceiver ) throws IOException {
		if ( isOpened() ) {
			if ( _loopbackTransceiver == aLoopbackTransceiver && _loopbackTransceiver.isOpened() ) {
				return;
			}
			throw new IOException( "Unable to open the connection is is is ALREADY OPEN; connection status is " + getConnectionStatus() + "." );
		}
		super.open();
		_loopbackTransceiver = aLoopbackTransceiver;
		if ( _loopbackTransceiver.isOpenable( this ) ) {
			_loopbackTransceiver.open( this );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitShort( short aShort ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Unable to write datagram <" + aShort + "> as the connection is NOT OPEN; connection status is " + getConnectionStatus() + "." );
		}
		_loopbackTransceiver.pushDatagram( aShort );
	}

	/**
	 * Pushes a datagram into the {@link LoopbackBytesReceiver}. Them datagrams
	 * can be retrieved via {@link #receiveShort()}: use {@link #available()} to
	 * test beforehand whether there is a datagram available.
	 *
	 * @param aDatagram The datagram to be pushed into the
	 *        {@link LoopbackShortsReceiver}; to be retrieved with the
	 *        {@link #receiveShort()} method.
	 * 
	 * @throws IOException the open exception
	 */
	@Override
	public void pushDatagram( short aDatagram ) throws IOException {
		super.pushDatagram( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_loopbackTransceiver.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void close() throws IOException {
		super.close();
		if ( _loopbackTransceiver != null && !_loopbackTransceiver.isClosed() ) {
			_loopbackTransceiver.close();
			_loopbackTransceiver = null;
		}
	}
}
