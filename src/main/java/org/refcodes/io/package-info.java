// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact defines basic types handling communication between processes
 * (across system boundaries), to be harnessed by complex and low level I/O,
 * such as the {@link java.io.InputStream} decorator types
 * {@link org.refcodes.io.ReplayInputStream},
 * {@link org.refcodes.io.FilterInputStream},
 * {@link org.refcodes.io.ClipboardInputStream},
 * {@link org.refcodes.io.ZipFileInputStream},
 * {@link org.refcodes.io.ReplaceInputStream} or
 * {@link org.refcodes.io.TimeoutInputStream} alongside
 * {@link org.refcodes.io.AvailableInputStream} as well as the
 * {@link java.io.OutputStream} decorator types
 * {@link org.refcodes.io.ClipboardOutputStream},
 * {@link org.refcodes.io.LineBreakOutputStream} or
 * {@link org.refcodes.io.ZipFileOutputStream}.
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-io"><strong>refcodes-io: Paving
 * the road for complex and low level I/O across boundaries </strong></a>
 * documentation for an up-to-date and detailed description on the usage of this
 * artifact.
 * </p>
 */
package org.refcodes.io;
