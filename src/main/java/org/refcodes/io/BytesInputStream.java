// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;

import org.refcodes.component.Closable;

/**
 * (as {@link java.io.InputStream} does not provide some useful *interface*)
 * 
 * @see java.io.InputStream
 */
public interface BytesInputStream extends Closable {

	/**
	 * Read.
	 *
	 * @return the int
	 * 
	 * @throws IOException the open exception
	 */
	int read() throws IOException;

	/**
	 * Read.
	 *
	 * @param b the b
	 * 
	 * @return the int
	 * 
	 * @throws IOException the open exception
	 */
	int read( byte b[] ) throws IOException;

	/**
	 * Read.
	 *
	 * @param b the b
	 * @param off the off
	 * @param len the len
	 * 
	 * @return the int
	 * 
	 * @throws IOException the open exception
	 */
	int read( byte b[], int off, int len ) throws IOException;

	/**
	 * Skip.
	 *
	 * @param n the n
	 * 
	 * @return the long
	 * 
	 * @throws IOException the open exception
	 */
	long skip( long n ) throws IOException;

	/**
	 * Availability.
	 *
	 * @return the int
	 * 
	 * @throws IOException the open exception
	 */
	int available() throws IOException;

	// void close() throws IOException;

	/**
	 * Mark.
	 *
	 * @param readlimit the readlimit
	 */
	void mark( int readlimit );

	/**
	 * Reset.
	 *
	 * @throws IOException the open exception
	 */
	void reset() throws IOException;

	/**
	 * Mark supported.
	 *
	 * @return true, if successful
	 */
	boolean markSupported();

}