// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;

import org.refcodes.numerical.InverseFunction;

/**
 * A {@link InverseInputStream} wraps an {@link InputStream} and consumes input
 * bytes by applying a {@link InverseFunction} on each byte read from the
 * provided {@link InputStream} before passing back the processed byte to the
 * caller. The input of the {@link InverseInputStream} can be converted back by
 * the according {@link BijectiveOutputStream}.
 */
public class InverseInputStream extends InputStream {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final InputStream _inputStream;
	private final InverseFunction<Byte, Byte> _inverseFunction;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link InverseInputStream} by wrapping the given
	 * {@link InputStream} for the provided {@link InverseFunction} to be
	 * applied on the bytes to be read.
	 * 
	 * @param aInputStream The {@link InputStream} to be wrapped.
	 * @param aInverseFunction The {@link InverseFunction} to be applied to the
	 *        bytes to be read.
	 */
	public InverseInputStream( InputStream aInputStream, InverseFunction<Byte, Byte> aInverseFunction ) {
		_inputStream = aInputStream;
		_inverseFunction = aInverseFunction;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _inputStream.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_inputStream.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object aObj ) {
		return _inputStream.equals( aObj );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return _inputStream.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mark( int aReadlimit ) {
		_inputStream.mark( aReadlimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported() {
		return _inputStream.markSupported();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException {
		return Byte.toUnsignedInt( _inverseFunction.applyInversion( (byte) _inputStream.read() ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() throws IOException {
		_inputStream.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip( long aArg0 ) throws IOException {
		return _inputStream.skip( aArg0 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _inputStream.toString();
	}
}
