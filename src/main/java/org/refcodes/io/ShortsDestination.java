// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@link ShortsDestination} is used to receive short blocks (arrays) in a
 * unified way. The {@link #receiveAllShorts()} method provides the next
 * available short block from the counterpart {@link DatagramsTransmitter} or
 * {@link DatagramTransmitter}; in case there is none available, then this
 * method halts until one is available.
 */
@FunctionalInterface
public interface ShortsDestination extends ShortDestination {

	/**
	 * {@inheritDoc}
	 */
	@Override
	short receiveShort() throws IOException;

	/**
	 * Reads (receives) the all currently available data.
	 * 
	 * @return The next short block sent from the {@link DatagramsTransmitter}
	 *         or {@link DatagramReceiver} counterpart.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	default short[] receiveAllShorts() throws IOException {
		return receiveShorts( -1 );
	}

	/**
	 * Similar to {@link #receiveAllShorts()} though at maximum the amount of
	 * data as provided returned.
	 * 
	 * @param aLength The block-size which is not to exceeded by the returned
	 *        data. A value of -1 specifies to retrieve all available datagrams
	 *        (same behavior as method {@link #receiveAllShorts()}.
	 * 
	 * @return The next short block sent from the {@link DatagramsTransmitter}
	 *         or {@link DatagramReceiver} counterpart.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	default short[] receiveShorts( int aLength ) throws IOException {
		final List<Short> theDatagrams = new ArrayList<>();
		try {
			if ( aLength > 0 ) {
				for ( int i = 0; i < aLength; i++ ) {
					theDatagrams.add( receiveShort() );
				}
			}
			else {
				while ( true ) {
					theDatagrams.add( receiveShort() );
				}
			}
		}
		catch ( IOException e ) {
			if ( theDatagrams.isEmpty() ) {
				throw e;
			}
		}
		// return toPrimitiveType( theDatagrams.toArray( new Short[theDatagrams.size()] ) );
		final Short[] aShorts = theDatagrams.toArray( new Short[theDatagrams.size()] );
		if ( aShorts == null ) {
			return null;
		}
		final short[] thePrimitives = new short[aShorts.length];
		for ( int i = 0; i < aShorts.length; i++ ) {
			thePrimitives[i] = aShorts[i].shortValue();
		}
		return thePrimitives;
	}

	/**
	 * Receives a short array with the number of bytes specified inserted at the
	 * given offset. This method blocks till a byte is available.
	 *
	 * @param aBuffer The short array where to store the shorts at.
	 * @param aOffset The offset where to start storing the received shorts.
	 * @param aLength The number of shorts to receive.
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a timeout) while
	 *         receiving.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	default void receiveShorts( short[] aBuffer, int aOffset, int aLength ) throws IOException {
		for ( int i = aOffset; i < aOffset + aLength; i++ ) {
			aBuffer[i] = receiveShort();
		}
	}
}
