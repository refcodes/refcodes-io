// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@link DatagramsDestination} is used to receive data blocks (arrays) in a
 * unified way. The {@link #receiveAll()} method provides the next available
 * short block from the counterpart {@link DatagramsTransmitter} or
 * {@link DatagramTransmitter}; in case there is none available, then this
 * method halts until one is available.
 *
 * @param <DATA> the generic type
 */
@FunctionalInterface
public interface DatagramsDestination<DATA extends Serializable> extends DatagramDestination<DATA> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	DATA receive() throws IOException;

	/**
	 * Reads (receives) the all currently available data.
	 * 
	 * @return The next short block sent from the {@link DatagramsTransmitter}
	 *         or {@link DatagramReceiver} counterpart.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	default DATA[] receiveAll() throws IOException {
		return receive( -1 );
	}

	/**
	 * Similar to {@link #receiveAll()} though at maximum the amount of data as
	 * provided returned.
	 *
	 * @param aLength The block-size which is not to exceeded by the returned
	 *        data. A value of -1 specifies to retrieve all available datagrams
	 *        (same behavior as method {@link #receiveAll()}.
	 * 
	 * @return The next short block sent from the {@link DatagramsTransmitter}
	 *         or {@link DatagramReceiver} counterpart.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	@SuppressWarnings("unchecked")
	default DATA[] receive( int aLength ) throws IOException {
		final List<DATA> theDatagrams = new ArrayList<>();
		try {
			if ( aLength > 0 ) {
				for ( int i = 0; i < aLength; i++ ) {
					theDatagrams.add( receive() );
				}
			}
			else {
				while ( true ) {
					theDatagrams.add( receive() );
				}
			}
		}
		catch ( IOException e ) {
			if ( theDatagrams.isEmpty() ) {
				throw e;
			}
		}
		return theDatagrams.toArray( (DATA[]) ( new Object[theDatagrams.size()] ) );
	}

	/**
	 * Receives a byte array with the number of datagrams specified inserted at
	 * the given offset. This method blocks till a datagrams is available.
	 *
	 * @param aBuffer The datagrams array where to store the datagrams at.
	 * @param aOffset The offset where to start storing the received datagrams.
	 * @param aLength The number of datagrams to receive.
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a timeout) while
	 *         receiving.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	default void receive( DATA[] aBuffer, int aOffset, int aLength ) throws IOException {
		for ( int i = aOffset; i < aOffset + aLength; i++ ) {
			aBuffer[i] = receive();
		}
	}
}
