// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * A {@link OutputStreamComposite} wraps multiple {@link OutputStream} instances
 * to which to dispatch any operation applied to the
 * {@link OutputStreamComposite} instance. This implementation does not use
 * threads so you may have take care on the order of the {@link OutputStream}
 * instances passed to the constructor(s) as the operations a dispatched to the
 * {@link OutputStream} instances in that order as they were provided. The
 * {@link OutputStreamComposite} is most useful when capturing data written to
 * an {@link OutputStream} e.g. with the {@link HexOutputStream} type.
 */
public class OutputStreamComposite extends OutputStream {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final OutputStream[] _outputStreams;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link OutputStreamComposite} with the given
	 * {@link OutputStream} instances to which to spread the data written to
	 * this {@link OutputStreamComposite} instance.
	 * 
	 * @param aOutputStreams The {@link OutputStream} instances to which all
	 *        operations are to be dispatched.
	 */
	public OutputStreamComposite( OutputStream... aOutputStreams ) {
		_outputStreams = aOutputStreams;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( int b ) throws IOException {
		for ( OutputStream eStream : _outputStreams ) {
			eStream.write( b );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( byte[] b ) throws IOException {
		for ( OutputStream eStream : _outputStreams ) {
			eStream.write( b );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( byte[] b, int off, int len ) throws IOException {
		for ( OutputStream eStream : _outputStreams ) {
			eStream.write( b, off, len );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		for ( OutputStream eStream : _outputStreams ) {
			eStream.flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		super.close();
		for ( OutputStream eStream : _outputStreams ) {
			eStream.close();
		}
	}
}
