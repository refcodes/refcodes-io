// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * The {@link InputStreamTap} decorates an {@link InputStreamTap} by tapping
 * (sniffing data from) an {@link InputStream} and besides returning the read
 * data writes it to the provided {@link OutputStream}. In conjunction with the
 * {@link HexOutputStream} the data read from the provided {@link InputStream}
 * via the {@link InputStreamTap} may be printed out as Hex codes for debugging
 * purposes.
 */
public class InputStreamTap extends InputStream implements Flushable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final InputStream _inputStream;
	private final OutputStream _outbutStream;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link InputStreamTap} decorating the provided
	 * {@link InputStream} with tap functionality delegating data to the
	 * provided {@link OutputStream}.
	 * 
	 * @param aInputStream The {@link InputStream} to be decorated.
	 * @param aOutbutStream The {@link OutputStream} where to also write the
	 *        read data (from the {@link InputStream}) to.
	 */
	public InputStreamTap( InputStream aInputStream, OutputStream aOutbutStream ) {
		_inputStream = aInputStream;
		_outbutStream = aOutbutStream;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException {
		int theByte = _inputStream.read();
		_outbutStream.write( theByte );
		_outbutStream.flush();
		return theByte;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] bytes ) throws IOException {
		int theLength = _inputStream.read( bytes );
		if ( theLength > 0 ) {
			_outbutStream.write( bytes, 0, theLength );
			_outbutStream.flush();
		}
		return theLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] bytes, int offset, int length ) throws IOException {
		int theLength = _inputStream.read( bytes, offset, length );
		if ( theLength > 0 ) {
			_outbutStream.write( bytes, offset, theLength );
			_outbutStream.flush();
		}
		return theLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] readAllBytes() throws IOException {
		byte[] theBytes = _inputStream.readAllBytes();
		_outbutStream.write( theBytes );
		_outbutStream.flush();
		return theBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] readNBytes( int length ) throws IOException {
		byte[] theBytes = _inputStream.readNBytes( length );
		_outbutStream.write( theBytes );
		_outbutStream.flush();
		return theBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int readNBytes( byte[] bytes, int offset, int length ) throws IOException {
		int theLength = _inputStream.readNBytes( bytes, offset, length );
		if ( theLength > 0 ) {
			_outbutStream.write( bytes, offset, theLength );
			_outbutStream.flush();
		}
		return theLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _inputStream.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip( long aN ) throws IOException {
		return _inputStream.skip( aN );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void skipNBytes( long aN ) throws IOException {
		_inputStream.skipNBytes( aN );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _inputStream.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mark( int aReadlimit ) {
		_inputStream.mark( aReadlimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() throws IOException {
		_inputStream.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported() {
		return _inputStream.markSupported();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long transferTo( OutputStream aOut ) throws IOException {
		return _inputStream.transferTo( aOut );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_outbutStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_inputStream.close();
		_outbutStream.flush();
		_outbutStream.close();
	}
}
