// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.Serializable;

/**
 * The {@link DatagramDestination} is used to receive datagrams in a unified
 * way. The {@link #receive()} method provides the next available datagram from
 * the counterpart {@link DatagramSource}; in case there is none available.
 *
 * @param <DATA> The type of the datagram to be operated with. In case you wish
 *        to use array types, you might better use the {@link DatagramsReceiver}
 *        type.
 */
@FunctionalInterface
public interface DatagramDestination<DATA extends Serializable> {

	/**
	 * Reads (receives) the next datagram passed from a
	 * {@link DatagramTransmitter} counterpart. In case none datagram is
	 * available, then this method blocks until one is available. To prevent
	 * blocking, use the {@link DatagramReceiver} extension's
	 * {@link DatagramReceiver#available()} method to test beforehand whether a
	 * byte is available (in a multi-threaded usage scenario,
	 * {@link DatagramReceiver#available()} is not a reliable indicator whether
	 * this method will block or not). When a {@link Thread} is waiting for a
	 * datagram to be read and {@link Thread#interrupt()} is being called, then
	 * the operation is aborted and an {@link InterruptedException} is thrown.
	 * 
	 * @return The next datagram sent from the {@link DatagramTransmitter}
	 *         counterpart.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 * 
	 * @throws EOFException Signals that an end of file or end of stream has
	 *         been reached unexpectedly during input.
	 */
	DATA receive() throws IOException;

}
