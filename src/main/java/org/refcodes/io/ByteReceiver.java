// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

/**
 * The {@link ByteReceiver} is used to receive bytes in a unified way. The
 * {@link #receiveByte()} method provides the next available datagram from the
 * counterpart {@link DatagramTransmitter}; in case there is none available,
 * then this method halts until one is available or the {@link ByteReceiver} is
 * being shut down or the underlying connection is closed. The
 * {@link #available()} method returns true in case a next datagram can be
 * fetched via the {@link #receiveByte()} method from the {@link ByteReceiver}
 * counterpart.
 */
public interface ByteReceiver extends ByteDestination, Receivable {}
