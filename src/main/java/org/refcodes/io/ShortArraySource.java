// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.refcodes.struct.ShortArrayAccessor;

/**
 * The Class ShortArraySource.
 */
public class ShortArraySource implements ShortsSource, ShortArrayAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final List<Short> _shortList = new ArrayList<>();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitShort( short aShort ) throws IOException {
		_shortList.add( aShort );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitShorts( short[] aShorts, int aOffset, int aLength ) throws IOException {
		for ( int i = aOffset; i < aLength; i++ ) {
			_shortList.add( aShorts[i] );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short[] getShorts() {
		return toPrimitiveType( _shortList.toArray( new Short[_shortList.size()] ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static short[] toPrimitiveType( Short[] aShorts ) {
		if ( aShorts == null ) {
			return null;
		}
		final short[] thePrimitives = new short[aShorts.length];
		for ( int i = 0; i < aShorts.length; i++ ) {
			thePrimitives[i] = aShorts[i].shortValue();
		}
		return thePrimitives;
	}
}
