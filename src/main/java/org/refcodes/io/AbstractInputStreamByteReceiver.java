// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import org.refcodes.component.AbstractConnectableAutomaton;
import org.refcodes.component.ConnectionComponent.ConnectionAutomaton;
import org.refcodes.component.ConnectionOpenable;
import org.refcodes.component.ConnectionStatus;
import org.refcodes.data.SleepLoopTime;

/**
 * Abstract implementation of the {@link BytesReceiver} interface. The
 * {@link #open(InputStream)}, {@link #setConnectionStatus(ConnectionStatus)}
 * and {@link #isOpenable()} methods are your hooks when extending this class.
 */
public abstract class AbstractInputStreamByteReceiver extends AbstractConnectableAutomaton implements BytesReceiver {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final LinkedList<Byte> _datagramQueue = new LinkedList<>();
	private InputStream _inputStream = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		if ( isClosed() ) {
			return 0;
		}
		if ( !_datagramQueue.isEmpty() ) {
			return _datagramQueue.size();
		}
		try {
			final int theWord = _inputStream.read();
			if ( theWord == -1 ) {
				close();
				synchronized ( _datagramQueue ) {
					_datagramQueue.notifyAll();
				}
				return 0;
			}
			_datagramQueue.add( (byte) ( theWord & 0xFF ) );
			return _datagramQueue.size();
		}
		//	catch ( IOException e ) {
		//		synchronized ( _datagramQueue ) {
		//			_datagramQueue.notifyAll();
		//		}
		//		throw new IOException( "Unable to test datagram availability (connection status is <" + getConnectionStatus() + ">).", e );
		//	}
		catch ( IOException ioe ) {
			if ( isClosed() ) {
				return 0;
			}
			synchronized ( _datagramQueue ) {
				_datagramQueue.notifyAll();
			}
			try {
				if ( isThrownAsOfAlreadyClosed( ioe ) ) {
					super.close();
				}
				else {
					close();
				}
			}
			catch ( IOException e ) {
				throw new IOException( "Unable to test datagram availability (connection status is <" + getConnectionStatus() + ">).", e );
			}
			throw new IOException( "Unable to test datagram availability (connection status is <" + getConnectionStatus() + ">).", ioe );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte receiveByte() throws IOException {
		if ( _datagramQueue.isEmpty() && isClosed() ) {
			throw new EOFException( "Connection is closed and no (more) data is available (connection status is <" + getConnectionStatus() + ">)." );
		}
		if ( _datagramQueue.isEmpty() && !isOpened() ) {
			throw new IOException( "Unable to read datagram  as the connection is NOT OPEN; connection status is <" + getConnectionStatus() + ">." );
		}
		if ( !hasAvailable() ) {
			throw new IOException( "Connection is closed and no (more) data is available!" );
		}
		Byte theDatagram = _datagramQueue.poll();
		while ( theDatagram == null ) {
			if ( !hasAvailable() ) {
				throw new IOException( "Connection is closed and no (more) data is available (connection status is <" + getConnectionStatus() + ">)." );
			}
			try {
				Thread.sleep( SleepLoopTime.MIN.getTimeMillis() );
			}
			catch ( InterruptedException ignore ) {}
			theDatagram = _datagramQueue.poll();
		}
		return theDatagram;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void close() throws IOException {
		if ( !isClosed() ) {
			super.close();
			try {
				_inputStream.close();
			}
			catch ( IOException e ) {
				if ( !isThrownAsOfAlreadyClosed( e ) ) {
					throw new IOException( "Unable to close receiver (connection status is <" + getConnectionStatus() + ">).", e );
				}
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Open, see {@link ConnectionOpenable#open(Object)}.
	 *
	 * @param aInputStream the input stream
	 * 
	 * @throws IOException the open exception
	 */
	protected synchronized void open( InputStream aInputStream ) throws IOException {
		if ( isOpened() ) {
			throw new IOException( "Unable to open the connection is is is ALREADY OPEN; connection status is <" + getConnectionStatus() + ">." );
		}
		_inputStream = aInputStream;
		// LOGGER.debug( "Starting I/O stream receiver daemon <" +
		// _ioStreamReceiverDaemon.getClass().getName() + ">." );
		setConnectionStatus( ConnectionStatus.OPENED );
	}

	/**
	 * Checks if is openable. See also
	 * {@link ConnectionAutomaton#isOpenable(Object)}.
	 *
	 * @param aInputStream the input stream
	 * 
	 * @return true, if is openable
	 */
	protected boolean isOpenable( InputStream aInputStream ) {
		return aInputStream == null ? false : !isOpened();
	}
}
