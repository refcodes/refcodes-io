// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Makes the wrapped {@link Iterator} serializable, else passing
 * {@link Iterator} instances would cause an {@link NotSerializableException} or
 * similar.
 *
 * @param <T> The type of the instances managed by the {@link Iterator}.
 */
public class SerializableIterator<T> implements Iterator<T>, Serializable {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private transient Iterator<T> _iterator = null;

	private final List<T> _list = new ArrayList<>();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new serializable iterator impl.
	 */
	public SerializableIterator() {}

	/**
	 * Instantiates a new serializable iterator impl.
	 *
	 * @param aIterator the iterator
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SerializableIterator( Iterator<T> aIterator ) {
		assert ( aIterator != null );
		T eObj;
		while ( aIterator.hasNext() ) {
			eObj = aIterator.next();
			if ( ( eObj instanceof ListIterator ) && ( !( eObj instanceof SerializableListIterator ) ) ) {
				eObj = (T) new SerializableListIterator( (ListIterator) eObj );
			}
			else if ( ( eObj instanceof Iterator ) && ( !( eObj instanceof SerializableIterator ) ) ) {
				eObj = (T) new SerializableIterator( (Iterator) eObj );
			}
			_list.add( eObj );
		}
		// _iterator = _list.iterator();
		// _list = null;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		lazyInit();
		return _iterator.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() {
		lazyInit();
		return _iterator.next();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		lazyInit();
		_iterator.remove();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Lazy init.
	 */
	private void lazyInit() {
		if ( _iterator == null ) {
			synchronized ( this ) {
				if ( _iterator == null ) {
					_iterator = _list.iterator();
				}
			}
		}
	}
}
