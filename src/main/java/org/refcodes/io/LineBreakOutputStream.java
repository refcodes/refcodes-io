// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.OutputStream;

import org.refcodes.runtime.SystemProperty;

/**
 * A {@link LineBreakOutputStream} wraps an {@link OutputStream} for adding line
 * breaks after a given number of bytes being written.
 */
public class LineBreakOutputStream extends OutputStream {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private long _count = 0;
	private final String _lineBreak;
	private final int _lineWidth;
	private final OutputStream _outputStream;
	private final boolean _isFlushLineBreak;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link LineBreakOutputStream} with the given arguments used
	 * for configuration.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be wrapped.
	 * @param aLineWidth The line width after which to insert a line break, a
	 *        line width of -1 does not insert any line breaks at all.
	 * @param aLineBreak The line break chars to use.
	 */
	public LineBreakOutputStream( OutputStream aOutputStream, int aLineWidth, String aLineBreak ) {
		_lineBreak = aLineBreak;
		_lineWidth = aLineWidth;
		_outputStream = aOutputStream;
		_isFlushLineBreak = false;
	}

	/**
	 * Constructs a {@link LineBreakOutputStream} with the given arguments used
	 * for configuration. The line break characters used are the ones used by
	 * the underlying operating system.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be wrapped.
	 * @param aLineWidth The line width after which to insert a line break, a
	 *        line width of -1 does not insert any line breaks at all.
	 */
	public LineBreakOutputStream( OutputStream aOutputStream, int aLineWidth ) {
		_lineBreak = SystemProperty.LINE_SEPARATOR.getValue();
		_lineWidth = aLineWidth;
		_outputStream = aOutputStream;
		_isFlushLineBreak = false;
	}

	/**
	 * Constructs a {@link LineBreakOutputStream} with the given arguments used
	 * for configuration.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be wrapped.
	 * @param aLineWidth The line width after which to insert a line break, a
	 *        line width of -1 does not insert any line breaks at all.
	 * @param aLineBreak The line break chars to use.
	 * @param isFlushLineBreak When true, then a line break is also inserted
	 *        upon a {@link #flush()} operation.
	 */
	public LineBreakOutputStream( OutputStream aOutputStream, int aLineWidth, String aLineBreak, boolean isFlushLineBreak ) {
		_lineBreak = aLineBreak;
		_lineWidth = aLineWidth;
		_outputStream = aOutputStream;
		_isFlushLineBreak = isFlushLineBreak;
	}

	/**
	 * Constructs a {@link LineBreakOutputStream} with the given arguments used
	 * for configuration. The line break characters used are the ones used by
	 * the underlying operating system.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be wrapped.
	 * @param aLineWidth The line width after which to insert a line break, a
	 *        line width of -1 does not insert any line breaks at all.
	 * @param isFlushLineBreak When true, then a line break is also inserted
	 *        upon a {@link #flush()} operation.
	 */
	public LineBreakOutputStream( OutputStream aOutputStream, int aLineWidth, boolean isFlushLineBreak ) {
		_lineBreak = SystemProperty.LINE_SEPARATOR.getValue();
		_lineWidth = aLineWidth;
		_outputStream = aOutputStream;
		_isFlushLineBreak = isFlushLineBreak;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( int aByte ) throws IOException {
		_count++;
		_outputStream.write( aByte );
		if ( _lineWidth != -1 ) {
			if ( _count % _lineWidth == 0 ) {
				_outputStream.write( _lineBreak.getBytes() );
				_count = 0;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		if ( _isFlushLineBreak ) {
			_outputStream.write( _lineBreak.getBytes() );
			_count = 0;
		}
		super.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( _lineWidth != -1 ) {
			if ( _count % _lineWidth != 0 ) {
				_outputStream.write( _lineBreak.getBytes() );
				_count = 0;
			}
			super.close();
		}
	}
}
