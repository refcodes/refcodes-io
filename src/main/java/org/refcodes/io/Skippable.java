// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;

import org.refcodes.data.IoTimeout;
import org.refcodes.exception.TimeoutIOException;

/**
 * The {@link Skippable} interface defines methods useful for omitting bytes
 * known to be unusable for the according operation.
 */
public interface Skippable {

	/**
	 * Clears the read buffer from all available bytes as of
	 * {@link #available()} at the time of this call. The bytes available are
	 * determined at the beginning of the skip process to prevent ongoing
	 * skipping of bytes on very slow machines and whilst receiving at very fast
	 * transfer rates (hypothetical).
	 * 
	 * @throws IOException thrown in case of I/O issues.
	 */
	default void skipAvailable() throws IOException {
		final int theAvailable = available();
		if ( theAvailable > 0 ) {
			skip( theAvailable );
		}
	}

	/**
	 * Skips all available bytes till after the given time no more bytes are
	 * available, else skipping is repeated till no more bytes are available
	 * within the given time span.This method blocks until the according
	 * available bytes have been skipped. ATTENTION: To apply a custom timeout,
	 * please use {@link #skipAvailableWithin(long, long)}.
	 * 
	 * @param aSkipTimeSpanInMs the skip time span in ms
	 * 
	 * @throws IOException thrown in case of I/O issues.
	 */
	default void skipAvailableWithin( long aSkipTimeSpanInMs ) throws IOException {
		skipAvailableWithin( aSkipTimeSpanInMs, -1 );
	}

	/**
	 * Skips all available bytes till after the given time no more bytes are
	 * available, else skipping is repeated till no more bytes are available
	 * within the given time span.
	 *
	 * @param aSkipTimeSpanInMs the skip time span in ms
	 * @param aTimeoutMillis the timeout in ms
	 * 
	 * @throws IOException thrown in case of I/O issues.
	 */
	default void skipAvailableWithin( long aSkipTimeSpanInMs, long aTimeoutMillis ) throws IOException {
		while ( available() > 0 ) {
			skip( available() );
			if ( available() == 0 ) {
				try {
					Thread.sleep( aSkipTimeSpanInMs );
				}
				catch ( InterruptedException ignore ) {}
			}
		}
	}

	/**
	 * Skips all available bytes till after the given time no more bytes are
	 * available, else skipping is repeated till no more bytes are available
	 * within the given time span.
	 *
	 * @param aSkipLoops the number of loops to skip available bytes.
	 * @param aSkipLoopTimeoutInMs the timeout in ms for each loop to wait.
	 * 
	 * @throws IOException thrown in case of I/O issues.
	 */
	default void skipAvailableWithin( int aSkipLoops, int aSkipLoopTimeoutInMs ) throws IOException {
		int i = 0;
		do {
			if ( available() == 0 ) {
				break;
			}
			try {
				Thread.sleep( aSkipLoopTimeoutInMs );
			}
			catch ( InterruptedException ignore ) {}
			skipAvailable();

		} while ( i++ < aSkipLoops );
	}

	/**
	 * Skips all available bytes till the given time span elapsed.
	 *
	 * @param aSkipTimeSpanInMs the skip time span in ms
	 * 
	 * @throws IOException thrown in case of I/O issues.
	 */
	default void skipAvailableTill( long aSkipTimeSpanInMs ) throws IOException {
		final long theStartTime = System.currentTimeMillis();
		final long theSleepLoopTime = IoTimeout.toTimeoutSleepLoopTimeInMs( aSkipTimeSpanInMs );
		while ( System.currentTimeMillis() - theStartTime < aSkipTimeSpanInMs ) {
			skipAvailable();
			synchronized ( this ) {
				try {
					wait( theSleepLoopTime );
				}
				catch ( InterruptedException ignore ) {}
			}
		}
	}

	/**
	 * Skips all available bytes till a given time span of silence is detected:
	 * The number of bytes available must be 0 for the given time span for this
	 * operation to conclude. This method blocks until the time span of silence
	 * has been reached. ATTENTION: To apply a custom timeout, please use
	 * {@link #skipAvailableTillSilenceFor(long, long)}.
	 * 
	 * @param aSilenceTimeSpanInMs The time span in milliseconds of silence (0
	 *        bytes available) till skipping is terminated.
	 * 
	 * @throws IOException thrown in case of I/O issues.
	 */
	default void skipAvailableTillSilenceFor( long aSilenceTimeSpanInMs ) throws IOException {
		skipAvailableTillSilenceFor( aSilenceTimeSpanInMs, -1 );
	}

	/**
	 * Skips all available bytes till a given time span of silence is detected:
	 * The number of bytes available must be 0 for the given time span for this
	 * operation to conclude. In case the read timeout is not -1 and the overall
	 * time while skipping bytes exceeds the read timeout a
	 * {@link TimeoutIOException} is thrown.
	 * 
	 * @param aSilenceTimeSpanInMs The time span in milliseconds of silence (0
	 *        bytes available) till skipping is terminated.
	 * @param aTimeoutMillis The time in milliseconds before this operation is
	 *        terminated in case no period of silence as been detected before.
	 *        With a value of -1 timeout handling is disabled (blocking mode).
	 * 
	 * @throws IOException thrown in case of I/O issues.
	 * @throws TimeoutIOException in case the the read timeout is not -1 and the
	 *         overall time while skipping bytes exceeds the read timeout .
	 */
	default void skipAvailableTillSilenceFor( long aSilenceTimeSpanInMs, long aTimeoutMillis ) throws IOException {
		final long theStartTime = System.currentTimeMillis();
		final long theSleepLoopTime = IoTimeout.toTimeoutSleepLoopTimeInMs( aSilenceTimeSpanInMs );

		do {
			while ( available() > 0 ) {
				if ( aTimeoutMillis != -1 && System.currentTimeMillis() - theStartTime >= aTimeoutMillis ) {
					throw new TimeoutIOException( aTimeoutMillis, "Unable to sense a silence time span of <" + aSilenceTimeSpanInMs + "> milliseconds within a timeout of <" + aTimeoutMillis + "> milliseconds!" );
				}
				skipAvailable();
				synchronized ( this ) {
					try {
						wait( theSleepLoopTime );
					}
					catch ( InterruptedException ignore ) {}
				}
			}
		} while ( !hasSenseSilenceFor( aSilenceTimeSpanInMs, theSleepLoopTime ) );
	}

	/**
	 * Skips all currently available bytes except the number of bytes specified.
	 * 
	 * @param aLength The number of bytes to leave in the {@link InputStream}.
	 * 
	 * @throws IOException thrown in case of problems while skipping.
	 */
	default void skipAvailableExcept( int aLength ) throws IOException {
		if ( available() > aLength ) {
			skip( available() - aLength );
		}
	}

	/**
	 * Checks for sense silence for.
	 *
	 * @param aSilenceTimeSpanInMs the a silence time span in ms
	 * @param aSleepLoopTime the a sleep loop time
	 * 
	 * @return true, if successful
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private boolean hasSenseSilenceFor( long aSilenceTimeSpanInMs, long aSleepLoopTime ) throws IOException {
		final long theStartTime = System.currentTimeMillis();
		while ( System.currentTimeMillis() - theStartTime < aSilenceTimeSpanInMs ) {
			if ( available() > 0 ) {
				return false;
			}
			synchronized ( this ) {
				try {
					wait( aSleepLoopTime );
				}
				catch ( InterruptedException ignore ) {}
			}
		}
		return true;
	}

	/**
	 * Skips over and discards {@code n} bytes of data from this input stream.
	 * The {@code skip} method may, for a variety of reasons, end up skipping
	 * over some smaller number of bytes, possibly {@code 0}. This may result
	 * from any of a number of conditions; reaching end of file before {@code n}
	 * bytes have been skipped is only one possibility. The actual number of
	 * bytes skipped is returned. If {@code n} is negative, the {@code skip}
	 * method for class {@code InputStream} always returns 0, and no bytes are
	 * skipped. Subclasses may handle the negative value differently.
	 *
	 * The {@code skip} method implementation of this class creates a byte array
	 * and then repeatedly reads into it until {@code n} bytes have been read or
	 * the end of the stream has been reached. Subclasses are encouraged to
	 * provide a more efficient implementation of this method. For instance, the
	 * implementation may depend on the ability to seek.
	 *
	 * @param n the number of bytes to be skipped.
	 * 
	 * @return the actual number of bytes skipped which might be zero.
	 * 
	 * @throws IOException if an I/O error occurs.
	 */
	long skip( long n ) throws IOException;

	/**
	 * Returns an estimate of the number of bytes that can be read (or skipped
	 * over) from this input stream without blocking, which may be 0, or 0 when
	 * end of stream is detected. The read might be on the same thread or
	 * another thread. A single read or skip of this many bytes will not block,
	 * but may read or skip fewer bytes.
	 *
	 * Note that while some implementations as of the {@code InputStream} will
	 * return the total number of bytes in the stream, many will not. It is
	 * never correct to use the return value of this method to allocate a buffer
	 * intended to hold all data in this stream.
	 *
	 * A subclass's implementation of this method may choose to throw an
	 * {@link IOException} if this input stream has been closed.
	 *
	 * The {@code available} method of {@code InputStream} always returns
	 * {@code 0}.
	 *
	 * @return an estimate of the number of bytes that can be read (or skipped
	 *         over) from this input stream without blocking or {@code 0} when
	 *         it reaches the end of the input stream.
	 * 
	 * @throws IOException if an I/O error occurs.
	 */
	int available() throws IOException;

}