// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;

import org.refcodes.component.ConnectionStatus;

/**
 * A {@link OutputStreamConnectionDatagramsTransmitter} is a
 * {@link DatagramTransmitter} connected through I/O streams with a
 * {@link InputStreamConnectionReceiver}. A {@link Socket} or a
 * {@link ServerSocket} may be used to retrieve the according I/O streams.
 *
 * @param <DATA> The type of the datagram to be operated with.
 */
public class OutputStreamConnectionDatagramsTransmitter<DATA extends Serializable> extends AbstractDatagramsTransmitter<DATA> implements ConnectionDatagramsTransmitter<DATA, OutputStream> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ObjectOutputStream _objectOutputStream = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void open( OutputStream aOutputStream ) throws IOException {
		if ( isOpened() ) {
			throw new IOException( "Unable to open the connection is is is ALREADY OPEN; connection status is " + getConnectionStatus() + "." );
		}
		try {
			if ( aOutputStream instanceof ObjectOutputStream ) {
				// -------------------------------------------------------------
				// Hack to enable single threaded pipe streams:
				// -------------------------------------------------------------
				_objectOutputStream = (ObjectOutputStream) aOutputStream;
			}
			else {
				if ( !( aOutputStream instanceof BufferedOutputStream ) ) {
					_objectOutputStream = new ObjectOutputStream( new BufferedOutputStream( aOutputStream ) );
				}
				else {
					_objectOutputStream = new ObjectOutputStream( aOutputStream );
				}
			}
			_objectOutputStream.flush();
		}
		catch ( IOException aException ) {
			throw new IOException( "Unable to open the I/O stream receiver as of a causing exception.", aException );
		}
		setConnectionStatus( ConnectionStatus.OPENED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable( OutputStream aOutputStream ) {
		return aOutputStream == null ? false : !isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		try {
			_objectOutputStream.flush();
		}
		catch ( IOException e ) {
			throw new IOException( "Unable to flush underlying output stream <" + _objectOutputStream + ">.", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void close() throws IOException {
		super.close();
		try {
			_objectOutputStream.close();
		}
		catch ( IOException aException ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA aDatagram ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Unable to write datagram <" + aDatagram.getClass().getName() + "> as the connection is NOT OPEN; connection status is " + getConnectionStatus() + "." );
		}
		synchronized ( _objectOutputStream ) {
			try {
				_objectOutputStream.writeObject( aDatagram );
				_objectOutputStream.flush();
			}
			catch ( IOException aException ) {
				if ( isClosed() ) {
					return;
				}
				else {
					try {
						close();
					}
					catch ( IOException e ) {
						throw new IOException( "Unable to close malfunctioning connection.", e );
					}
				}
				throw new IOException( aException );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

}
