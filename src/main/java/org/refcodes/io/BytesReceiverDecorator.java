// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;

import org.refcodes.component.Closable;

/**
 * The {@link BytesReceiverDecorator} decorates a {@link BytesDestination} with
 * the additional methods of a {@link BytesReceiver} making it easy to use a
 * {@link BytesDestination} wherever a {@link BytesReceiver} is expected. This
 * is a very plain implementation: {@link #available()} always returns true if
 * {@link #isOpened()} is true and as long as none of the
 * {@link #receiveByte()}, {@link #receiveAllBytes()} or
 * {@link #receiveBytes(int)} methods threw an {@link IOException} (
 * {@link InterruptedException}). This implementation actually behaves as if
 * {@link #close()} has been called after verifying {@link #available()} and
 * before any of the read methods have been called.
 */
public class BytesReceiverDecorator extends AbstractBytesReceiver {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final BytesDestination _byteProvider;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new byte receiver decorator.
	 *
	 * @param aByteProvider the byte provider
	 */
	public BytesReceiverDecorator( BytesDestination aByteProvider ) {
		_byteProvider = aByteProvider;
		try {
			open();
		}
		catch ( IOException ignore ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte receiveByte() throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		return _byteProvider.receiveByte();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized int available() throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		//	return _byteProvider.available();
		throw new UnsupportedOperationException( "The underlying type <" + BytesDestination.class.getName() + "> does not support means to determine avaialbility!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] receiveAllBytes() throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		return _byteProvider.receiveAllBytes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] receiveBytes( int aLength ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		return _byteProvider.receiveBytes( aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( _byteProvider instanceof Closable ) {
			( (Closable) _byteProvider ).close();
		}
		super.close();
	}
}
