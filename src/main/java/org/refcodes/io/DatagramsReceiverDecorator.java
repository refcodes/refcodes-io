// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.Serializable;

import org.refcodes.component.Closable;

/**
 * The {@link DatagramsReceiverDecorator} decorates a
 * {@link DatagramsDestination} with the additional methods of a
 * {@link DatagramsReceiver} making it easy to use a
 * {@link DatagramsDestination} wherever a {@link DatagramsReceiver} is
 * expected.
 *
 * @param <DATA> the generic type
 */
public class DatagramsReceiverDecorator<DATA extends Serializable> extends AbstractDatagramsReceiver<DATA> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final DatagramsDestination<DATA> _provider;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new receiver decorator.
	 *
	 * @param aProvider the provider
	 */
	public DatagramsReceiverDecorator( DatagramsDestination<DATA> aProvider ) {
		_provider = aProvider;
		try {
			open();
		}
		catch ( IOException ignore ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DATA receive() throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		return _provider.receive();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized int available() throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		//	return _provider.available();
		throw new UnsupportedOperationException( "The underlying type <" + DatagramsDestination.class.getName() + "> does not support means to determine avaialbility!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DATA[] receiveAll() throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		return _provider.receiveAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DATA[] receive( int aLength ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		return _provider.receive( aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( _provider instanceof Closable ) {
			( (Closable) _provider ).close();
		}
		super.close();
	}
}
