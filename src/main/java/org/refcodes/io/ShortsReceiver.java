// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.refcodes.data.SleepLoopTime;

/**
 * The Interface ShortsReceiver.
 */
public interface ShortsReceiver extends ShortsDestination, ShortReceiver {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default short[] receiveAllShorts() throws IOException {
		final List<Short> theData = new ArrayList<>();
		while ( hasAvailable() ) {
			theData.add( receiveShort() );
		}
		final short[] theShorts = new short[theData.size()];
		for ( int i = 0; i < theShorts.length; i++ ) {
			theShorts[i] = theData.get( i );
		}
		return theShorts;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default short[] receiveShorts( int aLength ) throws IOException {
		final short[] theBlock = new short[aLength];
		int i = 0;
		while ( hasAvailable() && i < aLength ) {
			theBlock[i] = receiveShort();
			i++;
		}
		if ( i == 0 ) {
			while ( !hasAvailable() ) {
				try {
					Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
				}
				catch ( InterruptedException ignore ) {}
				while ( hasAvailable() && i < aLength ) {
					theBlock[i] = receiveShort();
					i++;
				}
			}
		}
		if ( i == aLength ) {
			return theBlock;
		}
		return Arrays.copyOfRange( theBlock, 0, i );
	}
}
