// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.OutputStream;

import org.refcodes.numerical.BijectiveFunction;

/**
 * A {@link BijectiveOutputStream} wraps an {@link OutputStream} and produces
 * output bytes by applying a {@link BijectiveFunction} on each byte to be
 * written before delegating the processed vale to the given
 * {@link OutputStream}. The output of the {@link BijectiveOutputStream} can be
 * converted back by the according {@link InverseInputStream}.
 */
public class BijectiveOutputStream extends OutputStream {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final OutputStream _outputStream;
	private final BijectiveFunction<Byte, Byte> _bijectiveFunction;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link BijectiveOutputStream} by wrapping the given
	 * {@link OutputStream} for the provided {@link BijectiveFunction} to be
	 * applied on the bytes to be written.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be wrapped.
	 * @param aBijectiveFunction The {@link BijectiveFunction} to be applied to
	 *        the bytes to be written.
	 */
	public BijectiveOutputStream( OutputStream aOutputStream, BijectiveFunction<Byte, Byte> aBijectiveFunction ) {
		_outputStream = aOutputStream;
		_bijectiveFunction = aBijectiveFunction;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_outputStream.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object aObj ) {
		return _outputStream.equals( aObj );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_outputStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return _outputStream.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _outputStream.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( int value ) throws IOException {
		_outputStream.write( Byte.toUnsignedInt( _bijectiveFunction.applyBijection( (byte) value ) ) );
	}
}
