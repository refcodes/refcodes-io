// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;

import org.refcodes.component.Closable;
import org.refcodes.component.Flushable;

/**
 * The {@link ByteTransmitterDecorator} decorates a {@link BytesSource} with the
 * additional methods of a {@link BytesTransmitter} making it easy to use a
 * {@link BytesSource} wherever a {@link BytesTransmitter} is expected.
 */
public class ByteTransmitterDecorator extends AbstractBytesTransmitter {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final BytesSource _byteConsumer;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new byte sender decorator.
	 *
	 * @param aByteConsumer the byte consumer
	 */
	public ByteTransmitterDecorator( BytesSource aByteConsumer ) {
		_byteConsumer = aByteConsumer;
		try {
			open();
		}
		catch ( IOException ignore ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitByte( byte aByte ) throws IOException {
		_byteConsumer.transmitByte( aByte );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitBytes( byte[] aBytes ) throws IOException {
		_byteConsumer.transmitBytes( aBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitBytes( byte[] aBytes, int aOffset, int aLength ) throws IOException {
		_byteConsumer.transmitBytes( aBytes, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		if ( _byteConsumer instanceof Flushable ) {
			( (Flushable) _byteConsumer ).flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( _byteConsumer instanceof Closable ) {
			( (Closable) _byteConsumer ).close();
		}
		super.close();
	}
}
