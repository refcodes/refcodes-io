package org.refcodes.io;

import java.io.IOException;
import java.util.List;

import org.refcodes.struct.ShortArrayAccessor;

/**
 * The Class ShortArrayReceiver.
 */
public class ShortArrayReceiver extends AbstractShortsReceiver implements ShortArrayAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new short array receiver impl.
	 *
	 * @param aDatagrams the datagrams
	 */
	public ShortArrayReceiver( short[] aDatagrams ) {
		super( aDatagrams.length );
		try {
			open();
			pushDatagrams( aDatagrams );
		}
		catch ( IOException ignore ) {}
	}

	/**
	 * Instantiates a new short array receiver impl.
	 *
	 * @param aDatagrams the datagrams
	 */
	public ShortArrayReceiver( List<Short> aDatagrams ) {
		this( toPrimitiveType( aDatagrams.toArray( new Short[aDatagrams.size()] ) ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short[] getShorts() {
		return toPrimitiveType( _datagramQueue.toArray( new Short[_datagramQueue.size()] ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static short[] toPrimitiveType( Short[] aShorts ) {
		if ( aShorts == null ) {
			return null;
		}
		final short[] thePrimitives = new short[aShorts.length];
		for ( int i = 0; i < aShorts.length; i++ ) {
			thePrimitives[i] = aShorts[i].shortValue();
		}
		return thePrimitives;
	}
}
