// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;

/**
 * The {@link Availability} interface defines methods to test availability of
 * data.
 */
public interface Availability {

	/**
	 * Determines the number of available datagram from a
	 * {@link DatagramTransmitter}. Use the {@link DatagramDestination}
	 * extenison's {@link DatagramDestination#receive()} method for retrieving
	 * available datagrams.
	 * 
	 * @return The number of datagrams ready to be retrieved .
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	int available() throws IOException;

	/**
	 * Determines whether a datagram is available from a
	 * {@link DatagramTransmitter}. Use the {@link DatagramDestination}
	 * extenison's {@link DatagramDestination#receive()} method for retrieving
	 * the available datagram.
	 * 
	 * @return True in case there is a datagram ready to be retrieved .
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	default boolean hasAvailable() throws IOException {
		return available() > 0;
	}
}
