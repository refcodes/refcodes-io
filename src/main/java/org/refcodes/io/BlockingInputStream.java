// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;

import org.refcodes.data.IoTimeout;

/**
 * The {@link BlockingInputStream} wraps an {@link InputStream} adding blocking
 * functionality, e.g. when trying to read, the read operation is blocked till
 * {@link #available()} returns a value greater than 0. Use a
 * {@link AvailableInputStream} to combine the {@link BlockingInputStream} with
 * a timeout.
 */
public class BlockingInputStream extends InputStream {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final InputStream _inputStream;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link InputStream} blocking functionality
	 * 
	 * @param aInputStream The {@link InputStream} to be decorated accordingly.
	 */
	public BlockingInputStream( InputStream aInputStream ) {
		_inputStream = aInputStream;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException {
		while ( available() == 0 ) {
			try {
				Thread.sleep( IoTimeout.MIN.getTimeMillis() );
			}
			catch ( InterruptedException ignore ) {}
		}
		return _inputStream.read();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] aB ) throws IOException {
		while ( available() == 0 ) {
			try {
				Thread.sleep( IoTimeout.MIN.getTimeMillis() );
			}
			catch ( InterruptedException ignore ) {}
		}
		return _inputStream.read( aB );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] aB, int aOff, int aLen ) throws IOException {
		while ( available() == 0 ) {
			try {
				Thread.sleep( IoTimeout.MIN.getTimeMillis() );
			}
			catch ( InterruptedException ignore ) {}
		}
		return _inputStream.read( aB, aOff, aLen );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] readAllBytes() throws IOException {
		while ( available() == 0 ) {
			try {
				Thread.sleep( IoTimeout.MIN.getTimeMillis() );
			}
			catch ( InterruptedException ignore ) {}
		}
		return _inputStream.readAllBytes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int readNBytes( byte[] aB, int aOff, int aLen ) throws IOException {
		while ( available() == 0 ) {
			try {
				Thread.sleep( IoTimeout.MIN.getTimeMillis() );
			}
			catch ( InterruptedException ignore ) {}
		}
		return _inputStream.readNBytes( aB, aOff, aLen );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _inputStream.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		super.close();
		_inputStream.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void mark( int readlimit ) {
		_inputStream.mark( readlimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void reset() throws IOException {
		_inputStream.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported() {
		return _inputStream.markSupported();
	}
}
