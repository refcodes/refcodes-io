// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import org.refcodes.mixin.InputStreamAccessor;

/**
 * Provides an accessor for an timeout input stream property.
 */
public interface TimeoutInputStreamAccessor extends InputStreamAccessor {

	/**
	 * Retrieves the timeout input stream from the timeout input stream
	 * property.
	 * 
	 * @param aTimeoutMillis The default timeout for read operations not
	 *        explicitly called with a timeout argument. With a value of -1
	 *        timeout handling is disabled (blocking mode).
	 * 
	 * @return The timeout input stream stored by the timeout input stream
	 *         property.
	 */
	TimeoutInputStream getInputStream( long aTimeoutMillis );

	/**
	 * Provides a mutator for an timeout input stream property.
	 */
	public interface TimeoutInputStreamMutator {

		/**
		 * Sets the timeout input stream for the timeout input stream property.
		 * 
		 * @param aTimeoutInputStream The timeout input stream to be stored by
		 *        the input stream property.
		 */
		void setInputStream( TimeoutInputStream aTimeoutInputStream );
	}

	/**
	 * Provides a mutator for an timeout input stream property.
	 * 
	 * @param <B> The builder which implements the
	 *        {@link TimeoutInputStreamBuilder}.
	 */
	public interface TimeoutInputStreamBuilder<B extends TimeoutInputStreamBuilder<?>> {

		/**
		 * Sets the timeout input stream to use and returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aTimeoutInputStream The timeout input stream to be stored by
		 *        the input stream property.
		 * 
		 * @return This {@link TimeoutInputStreamBuilder} instance to continue
		 *         configuration.
		 */
		B withInputStream( TimeoutInputStream aTimeoutInputStream );
	}

	/**
	 * Provides an timeout input stream property.
	 */
	public interface TimeoutInputStreamProperty extends TimeoutInputStreamAccessor, TimeoutInputStreamMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link TimeoutInputStream} (setter) as of
		 * {@link #setInputStream(TimeoutInputStream)} and returns the very same
		 * value (getter).
		 * 
		 * @param aTimeoutInputStream The {@link TimeoutInputStream} to set (via
		 *        {@link #setInputStream(TimeoutInputStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default TimeoutInputStream letTimeoutInputStream( TimeoutInputStream aTimeoutInputStream ) {
			setInputStream( aTimeoutInputStream );
			return aTimeoutInputStream;
		}
	}
}
