// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;

/**
 * Represents an {@link OutputStream} to a provided {@link File}: In case the
 * file points to a ZIP compressed file, then a therein to be contained file
 * with the same name excluding the ".zip" extension is created by the
 * {@link OutputStream}.
 */
public class ZipFileOutputStream extends BufferedOutputStream {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new zip file output stream impl.
	 *
	 * @param parent the parent
	 * @param child the child
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileOutputStream( File parent, String child ) throws IOException {
		this( new File( parent, child ) );
	}

	/**
	 * Instantiates a new zip file output stream impl.
	 *
	 * @param parent the parent
	 * @param child the child
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileOutputStream( String parent, String child ) throws IOException {
		this( new File( parent, child ) );
	}

	/**
	 * Instantiates a new zip file output stream impl.
	 *
	 * @param pathname the pathname
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileOutputStream( String pathname ) throws IOException {
		this( new File( pathname ) );
	}

	/**
	 * Instantiates a new zip file output stream impl.
	 *
	 * @param uri the uri
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileOutputStream( URI uri ) throws IOException {
		this( new File( uri ) );
	}

	/**
	 * Instantiates a new zip file output stream impl.
	 *
	 * @param aFile the file
	 * 
	 * @throws ZipException the zip exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ZipFileOutputStream( File aFile ) throws IOException {
		super( toOutputStream( aFile ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns an {@link OutputStream} to the provided {@link File}. In case the
	 * file points to a ZIP compressed file (it has the file suffix ".zip"),
	 * then the data written to the {@link OutputStream} is being ZIP
	 * compressed.
	 * 
	 * @param aFile The {@link File} for which to get the {@link OutputStream}.
	 * 
	 * @return An {@link OutputStream}, in case of a ZIP compressed {@link File}
	 *         was specified (with suffix ".zip"), then a compressed
	 *         {@link OutputStream} is returned.
	 * 
	 * @throws ZipException in case there were problems when accessing the ZIP
	 *         compressed {@link File}.
	 * @throws IOException in case there were problems working with the
	 *         {@link File}.
	 * @throws FileNotFoundException in case there was none such {@link File}
	 *         found.
	 */
	protected static OutputStream toOutputStream( File aFile ) throws IOException {
		final String theUnZipFileName = ZipUtility.toFileNameFromZip( aFile.getName() );
		if ( theUnZipFileName != null ) {
			final OutputStream theFileOutputStream = new BufferedOutputStream( new FileOutputStream( aFile ) );
			final ZipOutputStream theZipOutputStream = new ZipOutputStream( theFileOutputStream );
			final ZipEntry theZipEntry = new ZipEntry( theUnZipFileName );
			theZipOutputStream.putNextEntry( theZipEntry );
			return theZipOutputStream;
		}
		return new BufferedOutputStream( new FileOutputStream( aFile ) );
	}
}
