// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.refcodes.data.FilenameExtension;

/**
 * Utility class for some ZIP compression related issues.
 */
public class ZipUtility {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	private ZipUtility() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * Returns an {@link InputStream} from the provided {@link File}. In case
	 * the file points to a ZIP compressed file, then the uncompressed data is
	 * provided by the {@link InputStream}.
	 * 
	 * @param aFile The {@link File} for which to get the {@link InputStream}.
	 * 
	 * @return An {@link InputStream}, in case of a ZIP compressed {@link File},
	 *         an uncompressed {@link InputStream} is returned.
	 * 
	 * @throws ZipException in case there were problems when accessing the ZIP
	 *         compressed {@link File}.
	 * @throws IOException in case there were problems working with the
	 *         {@link File}.
	 * @throws FileNotFoundException in case there was none such {@link File}
	 *         found.
	 */
	@SuppressWarnings("resource")
	public static InputStream toInputStream( File aFile ) throws IOException {
		final String theUnZipFileName = toFileNameFromZip( aFile.getName() );
		if ( theUnZipFileName != null ) {
			final ZipFile theZipFile = new ZipFile( aFile );
			if ( theZipFile.size() != 1 ) {
				throw new ZipException( "The file \"" + aFile.getAbsolutePath() + "\" has <" + theZipFile.size() + "\" entries, expecting exactly one entry with name \"" + theUnZipFileName + "\"!" );
			}
			final Enumeration<?> e = theZipFile.entries();
			final ZipEntry theEntry = (ZipEntry) e.nextElement();
			if ( !theUnZipFileName.equals( theEntry.getName() ) ) {
				throw new ZipException( "The file \"" + aFile.getAbsolutePath() + "\" contains an entry with name \"" + theEntry.getName() + "\", though expecting entry with name \"" + theUnZipFileName + "\"!" );
			}
			return new BufferedInputStream( theZipFile.getInputStream( theEntry ) );
		}
		return new BufferedInputStream( new FileInputStream( aFile ) );
	}

	/**
	 * Truncates the ".zip" suffix from the filename and returns the result. For
	 * example a file with name "log-2023-07-12.txt.zip" results in
	 * "log-2023-07-12.txt".
	 * 
	 * @param aZipFileName The file name of the ZIP file for which to get the
	 *        "inner" file name.
	 * 
	 * @return The "inner" file name if the file suffix was ".zip", else null.
	 */
	public static String toFileNameFromZip( String aZipFileName ) {
		if ( aZipFileName.toLowerCase().endsWith( FilenameExtension.ZIP.getFilenameSuffix() ) ) {
			return aZipFileName.substring( 0, aZipFileName.length() - FilenameExtension.ZIP.getFilenameSuffix().length() );
		}
		return null;
	}
}
