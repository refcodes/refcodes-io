// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;

import org.refcodes.component.Closable;

/**
 * The {@link ShortsReceiverDecorator} decorates a {@link ShortsDestination}
 * with the additional methods of a {@link ShortsReceiver} making it easy to use
 * a {@link ShortsDestination} wherever a {@link ShortsReceiver} is expected.
 * This is a very plain implementation: {@link #available()} always returns true
 * if {@link #isOpened()} is true and as long as none of the
 * {@link #receiveShort()}, {@link #receiveAllShorts()} or
 * {@link #receiveShorts(int)} methods threw an {@link IOException} (
 * {@link InterruptedException}). This implementation actually behaves as if
 * {@link #close()} has been called after verifying {@link #available()} and
 * before any of the read methods have been called.
 */
public class ShortsReceiverDecorator extends AbstractShortsReceiver {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final ShortsDestination _shortProvider;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new short receiver decorator.
	 *
	 * @param aShortProvider the short provider
	 */
	public ShortsReceiverDecorator( ShortsDestination aShortProvider ) {
		_shortProvider = aShortProvider;
		try {
			open();
		}
		catch ( IOException ignore ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short receiveShort() throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		return _shortProvider.receiveShort();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized int available() throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		//	return _shortProvider.avaialble();
		throw new UnsupportedOperationException( "The underlying type <" + BytesDestination.class.getName() + "> does not support means to determine avaialbility!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short[] receiveAllShorts() throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		return _shortProvider.receiveAllShorts();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short[] receiveShorts( int aLength ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot read as this receiver is in connection status <" + getConnectionStatus() + ">." );
		}
		return _shortProvider.receiveShorts( aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( _shortProvider instanceof Closable ) {
			( (Closable) _shortProvider ).close();
		}
		super.close();
	}
}
