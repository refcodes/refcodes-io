// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.component.ConnectionStatus;

/**
 * The Class PrefetchBidirectionalStreamByteTransceiver.
 *
 * @author steiner
 */
public class PrefetchBidirectionalStreamByteTransceiver implements BytesTransceiver {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final BidirectionalStreamConnectionByteTransceiver _transceiver;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new prefetch bidirectional stream byte transceiver impl.
	 *
	 * @param aInputStream the input stream
	 * @param aOutputStream the output stream
	 * 
	 * @throws IOException the open exception
	 */
	public PrefetchBidirectionalStreamByteTransceiver( InputStream aInputStream, OutputStream aOutputStream ) throws IOException {
		_transceiver = new PrefetchBidirectionalStreamConnectionByteTransceiver();
		_transceiver.open( aInputStream, aOutputStream );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosed() {
		return _transceiver.isClosed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		return _transceiver.isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _transceiver.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] receiveAllBytes() throws IOException {
		return _transceiver.receiveAllBytes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _transceiver.getConnectionStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_transceiver.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte receiveByte() throws IOException {
		return _transceiver.receiveByte();
	}

	/**
	 * Write datagrams.
	 *
	 * @param aDatagram the datagram
	 * 
	 * @throws IOException the open exception
	 */
	@Override
	public void transmitBytes( byte[] aDatagram ) throws IOException {
		_transceiver.transmitBytes( aDatagram );
	}

	/**
	 * Write datagrams.
	 *
	 * @param aDatagram the datagram
	 * @param aOffset the offset
	 * @param aLength the length
	 * 
	 * @throws IOException the open exception
	 */
	@Override
	public void transmitBytes( byte[] aDatagram, int aOffset, int aLength ) throws IOException {
		_transceiver.transmitBytes( aDatagram, aOffset, aLength );
	}

	/**
	 * Write datagram.
	 *
	 * @param aByte the datagram
	 * 
	 * @throws IOException the open exception
	 */
	@Override
	public void transmitByte( byte aByte ) throws IOException {
		_transceiver.transmitByte( aByte );
	}

	/**
	 * Flush.
	 *
	 * @throws IOException the open exception
	 */
	@Override
	public void flush() throws IOException {
		_transceiver.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosable() {
		return _transceiver.isClosable();
	}
}
