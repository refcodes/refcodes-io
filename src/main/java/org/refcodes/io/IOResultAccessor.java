// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import org.refcodes.data.IoSleepLoopTime;
import org.refcodes.exception.TimeoutIOException;
import org.refcodes.mixin.ResultAccessor;

/**
 * Extends the {@link ResultAccessor} interface with an I/O timeout mechanism.
 *
 * @param <RES> The type of the result to be used.
 * @param <EXC> The exception thrown in case no result is available. Use
 *        {@link RuntimeException} to prevent forcing a try/catch block.
 */
public interface IOResultAccessor<RES, EXC extends Exception> extends ResultAccessor<RES, EXC> {

	/**
	 * Waits for the result. This method blocks till a result is available, the
	 * timeout has run out or an exception has been thrown in case there is none
	 * such result. Use {@link #hasResult()} to test beforehand whether we
	 * already have a result.
	 *
	 * @param aTimeoutMillis The timeout to wait for a result
	 * 
	 * @throws InterruptedException the interrupted exception
	 * @throws TimeoutIOException thrown in ca
	 */
	default void waitForResult( long aTimeoutMillis ) throws InterruptedException, TimeoutIOException {
		final long theStartTime = System.currentTimeMillis();
		long theWaitTimeInMs = aTimeoutMillis / 100;
		if ( theWaitTimeInMs < IoSleepLoopTime.MIN.getTimeMillis() ) {
			theWaitTimeInMs = IoSleepLoopTime.MIN.getTimeMillis();
		}
		if ( theWaitTimeInMs > aTimeoutMillis ) {
			theWaitTimeInMs = aTimeoutMillis;
		}
		while ( !hasResult() && System.currentTimeMillis() - theStartTime < aTimeoutMillis ) {
			synchronized ( this ) {
				wait( theWaitTimeInMs );
			}
		}
		if ( !hasResult() ) {
			//	try {
			//		if ( getResult() != null ) return;
			//	}
			//	catch ( Exception ignore ) {}

			if ( !hasResult() ) {
				throw new TimeoutIOException( aTimeoutMillis, "Unable to retrieve a result after a timeout of <" + aTimeoutMillis + "> milliseconds." );
			}
		}
	}
}
