// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

/**
 * Provides an accessor for a maximum connections property.
 */
public interface MaxConnectionsAccessor {

	/**
	 * Retrieves the maximum connections from the maximum connections property.
	 * 
	 * @return The maximum connections stored by the maximum connections
	 *         property.
	 */
	int getMaxConnections();

	/**
	 * Provides a mutator for a maximum connections property.
	 */
	public interface MaxConnectionsMutator {

		/**
		 * Sets the maximum connections for the maximum connections property.
		 * 
		 * @param aMaxConnections The maximum connections to be stored by the
		 *        maximum connections property.
		 */
		void setMaxConnections( int aMaxConnections );
	}

	/**
	 * Provides a builder method for a maximum connections property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MaxConnectionsBuilder<B extends MaxConnectionsBuilder<B>> {

		/**
		 * Sets the maximum connections for the maximum connections property.
		 * 
		 * @param aMaxConnections The maximum connections to be stored by the
		 *        maximum connections property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMaxConnections( int aMaxConnections );
	}

	/**
	 * Provides a maximum connections property.
	 */
	public interface MaxConnectionsProperty extends MaxConnectionsAccessor, MaxConnectionsMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setMaxConnections(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aMaxConnections The integer to set (via
		 *        {@link #setMaxConnections(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letMaxConnections( int aMaxConnections ) {
			setMaxConnections( aMaxConnections );
			return aMaxConnections;
		}
	}
}
