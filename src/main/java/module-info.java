module org.refcodes.io {
	requires org.refcodes.controlflow;
	requires org.refcodes.data;
	requires org.refcodes.numerical;
	requires transitive org.refcodes.textual;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.struct;
	requires org.refcodes.runtime;

	exports org.refcodes.io;
}
