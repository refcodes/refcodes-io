// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.Serializable;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * Tests the {@link PrefetchBidirectionalStreamConnectionTransceiver} and the
 * parts {@link PrefetchInputStreamConnectionReceiver} and
 * {@link OutputStreamConnectionDatagramsTransmitter}.
 */
public class IoStreamConnectionTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String DATAGRAM_A = "Datagram A";
	private static final String DATAGRAM_B = "Datagram B";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Tests the {@link PrefetchBidirectionalStreamConnectionTransceiver}.
	 *
	 * @throws IOException the open exception
	 * @throws InterruptedException the interrupted exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testIoStreamTransceiver() throws IOException, InterruptedException {
		final PipedInputStream theInputStreamA = new PipedInputStream();
		final PipedOutputStream theOutputStreamB = new PipedOutputStream( theInputStreamA );
		final PipedInputStream theInputStreamB = new PipedInputStream();
		final PipedOutputStream theOutputStreamA = new PipedOutputStream( theInputStreamB );
		// OPEN:
		final PrefetchBidirectionalStreamConnectionTransceiver<Serializable> theIoStreamTransceiverA = new PrefetchBidirectionalStreamConnectionTransceiver<>();
		final PrefetchBidirectionalStreamConnectionTransceiver<Serializable> theIoStreamTransceiverB = new PrefetchBidirectionalStreamConnectionTransceiver<>();
		// ---------------------------------------------------------------------
		// Hack to enable single threaded pipe streams:
		// ---------------------------------------------------------------------
		final ObjectOutputStream theObjectOutputStreamA = new ObjectOutputStream( theOutputStreamA );
		theObjectOutputStreamA.flush();
		final ObjectOutputStream theObjectOutputStreamB = new ObjectOutputStream( theOutputStreamB );
		theObjectOutputStreamB.flush();
		// ---------------------------------------------------------------------
		theIoStreamTransceiverA.open( theInputStreamA, theObjectOutputStreamA );
		theIoStreamTransceiverB.open( theInputStreamB, theObjectOutputStreamB );
		// WRITE FROM A TO B:
		theIoStreamTransceiverA.transmit( DATAGRAM_A );
		final String theDatagramA = (String) theIoStreamTransceiverB.receive();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "DatagramsTransceiver B read datagram \"" + theDatagramA + "\" from transceiver A." );
		}
		assertEquals( DATAGRAM_A, theDatagramA );
		// WRITE FROM B TO A:
		theIoStreamTransceiverB.transmit( DATAGRAM_B );
		final String theDatagramB = (String) theIoStreamTransceiverA.receive();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "DatagramsTransceiver A read datagram \"" + theDatagramB + "\" from transceiver B." );
		}
		assertEquals( DATAGRAM_B, theDatagramB );
		// CLOSE:
		theIoStreamTransceiverA.close();
		theIoStreamTransceiverB.close();
		assertTrue( theIoStreamTransceiverA.isClosed() );
		assertTrue( theIoStreamTransceiverB.isClosed() );
		try {
			theIoStreamTransceiverA.transmit( DATAGRAM_A );
			fail( "Should not reach this code!" );
		}
		catch ( IOException aException ) { /* Expected */}
		try {
			theIoStreamTransceiverA.receive();
			fail( "Should not reach this code!" );
		}
		catch ( IOException aException ) { /* Expected */}
		try {
			theIoStreamTransceiverB.transmit( DATAGRAM_A );
			fail( "Should not reach this code!" );
		}
		catch ( IOException aException ) { /* Expected */}
		try {
			theIoStreamTransceiverB.receive();
			fail( "Should not reach this code!" );
		}
		catch ( IOException aException ) { /* Expected */}
	}

	@Test
	public void testIoStreamSenderAndReceiver() throws IOException, InterruptedException {
		final PipedInputStream theInputStream = new PipedInputStream();
		final PipedOutputStream theOutputStream = new PipedOutputStream( theInputStream );
		// ---------------------------------------------------------------------
		// Hack to enable single threaded pipe streams:
		// ---------------------------------------------------------------------
		final ObjectOutputStream theObjectOutputStream = new ObjectOutputStream( theOutputStream );
		theObjectOutputStream.flush();
		// ---------------------------------------------------------------------
		// OPEN:
		final PrefetchInputStreamConnectionReceiver<Serializable> theIoStreamReceiver = new PrefetchInputStreamConnectionReceiver<>();
		theIoStreamReceiver.open( theInputStream );
		final OutputStreamConnectionDatagramsTransmitter<Serializable> theIoStreamSender = new OutputStreamConnectionDatagramsTransmitter<>();
		theIoStreamSender.open( theObjectOutputStream );
		// WRITE A AND B:
		theIoStreamSender.transmit( DATAGRAM_A );
		theIoStreamSender.transmit( DATAGRAM_B );
		String theDatagram = (String) theIoStreamReceiver.receive();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "DatagramsReceiver read datagram \"" + theDatagram + "\" from sender." );
		}
		assertEquals( DATAGRAM_A, theDatagram );
		theDatagram = (String) theIoStreamReceiver.receive();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "DatagramsReceiver read datagram \"" + theDatagram + "\" from sender." );
		}
		assertEquals( DATAGRAM_B, theDatagram );
		// CLOSE:
		theIoStreamSender.close();
		theIoStreamReceiver.close();
		assertTrue( theIoStreamSender.isClosed() );
		assertTrue( theIoStreamReceiver.isClosed() );
		try {
			theIoStreamSender.transmit( DATAGRAM_A );
			fail( "Should not reach this code!" );
		}
		catch ( IOException aException ) { /* Expected */}
		try {
			theIoStreamReceiver.receive();
			fail( "Should not reach this code!" );
		}
		catch ( IOException aException ) { /* Expected */}
	}
}
