// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

import org.junit.jupiter.api.Test;
import org.refcodes.exception.TimeoutIOException;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.HorizAlignTextMode;

public class TimeoutInputStreamTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testTimeoutInputStream1() throws IOException {
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( new byte[0] );
		try ( TimeoutInputStream theTimeoutStream = new TimeoutInputStream( theInputStream, 500 ); ) {
			final int theValue = theTimeoutStream.read();
			assertEquals( -1, theValue );
		}
	}

	@Test
	public void testTimeoutInputStream2() throws IOException {
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( new byte[0] );
		try ( TimeoutInputStream theTimeoutStream = new TimeoutInputStream( theInputStream ); ) {
			final int theValue = theTimeoutStream.read( 500 );
			assertEquals( -1, theValue );
		}
	}

	@Test
	public void testTimeoutInputStream3() throws IOException {
		final byte[] theInput = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theInput );
		try ( TimeoutInputStream theTimeoutStream = new TimeoutInputStream( theInputStream, 500 ) ) {
			final byte[] theResult = new byte[10];
			theTimeoutStream.read( theResult );
			assertArrayEquals( theInput, theResult );
		}
	}

	@Test
	public void testTimeoutInputStream4() throws IOException {
		final byte[] theInput = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theInput );
		try ( TimeoutInputStream theTimeoutStream = new TimeoutInputStream( theInputStream ) ) {
			final byte[] theResult = new byte[10];
			theTimeoutStream.read( theResult, 500 );
			assertArrayEquals( theInput, theResult );
		}
	}

	@Test
	public void testTimeoutInputStream5() throws IOException {
		final byte[] theInput = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theInput );
		try ( TimeoutInputStream theTimeoutStream = new TimeoutInputStream( theInputStream, 500 ) ) {
			final int theValue = theTimeoutStream.read( new byte[11] );
			assertEquals( 10, theValue );
		}
	}

	@Test
	public void testTimeoutInputStream6() throws IOException {
		final byte[] theInput = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theInput );
		try ( TimeoutInputStream theTimeoutStream = new TimeoutInputStream( theInputStream ) ) {
			final int theValue = theTimeoutStream.read( new byte[11], 500 );
			assertEquals( 10, theValue );
		}
	}

	@Test
	public void testTimeoutBufferedReader1() throws IOException {
		try ( BufferedReader theReader = new BufferedReader( new InputStreamReader( new TimeoutInputStream( new FileInputStream( "src/test/resources/arecibo.txt" ), 100000 ) ) ) ) {
			String eLine;
			int count = 0;
			int ones = 0;
			int zeros = 0;
			while ( ( eLine = theReader.readLine() ) != null ) {
				for ( int i = 0; i < eLine.length(); i++ ) {
					if ( eLine.charAt( i ) == '0' ) {
						zeros++;
					}
					if ( eLine.charAt( i ) == '1' ) {
						ones++;
					}
				}
				count++;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eLine );
				}
			}
			assertEquals( 73, count );
			assertEquals( 1679, zeros + ones );
		}
	}

	@Test
	public void testTimeoutBufferedReader2() throws IOException {
		try ( BufferedReader theReader = new BufferedReader( new InputStreamReader( new BufferedInputStream( new TimeoutInputStream( new FileInputStream( "src/test/resources/arecibo.txt" ), 100000 ) ) ) ) ) {
			String eLine;
			int count = 0;
			int ones = 0;
			int zeros = 0;
			while ( ( eLine = theReader.readLine() ) != null ) {
				for ( int i = 0; i < eLine.length(); i++ ) {
					if ( eLine.charAt( i ) == '0' ) {
						zeros++;
					}
					if ( eLine.charAt( i ) == '1' ) {
						ones++;
					}
				}
				count++;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eLine );
				}
			}
			assertEquals( 73, count );
			assertEquals( 1679, zeros + ones );
		}
	}

	@Test
	public void testTimeoutBufferedReader3() throws IOException {
		try ( BufferedReader theReader = new BufferedReader( new InputStreamReader( new BufferedInputStream( new TimeoutInputStream( new BufferedInputStream( new FileInputStream( "src/test/resources/arecibo.txt" ) ), 1000 ) ) ) ) ) {
			String eLine;
			int count = 0;
			int ones = 0;
			int zeros = 0;
			while ( ( eLine = theReader.readLine() ) != null ) {
				for ( int i = 0; i < eLine.length(); i++ ) {
					if ( eLine.charAt( i ) == '0' ) {
						zeros++;
					}
					if ( eLine.charAt( i ) == '1' ) {
						ones++;
					}
				}
				count++;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eLine );
				}
			}
			assertEquals( 73, count );
			assertEquals( 1679, zeros + ones );
		}
	}

	@Test
	public void testBlockingInputStream() throws IOException {
		try ( InputStream theInput = new TimeoutInputStream( new BlockingRandomInputStream( 128, 2000 ), 1000 ) ) {
			int eValue;
			int count = 0;
			final long startTime = System.currentTimeMillis();
			try {
				while ( ( eValue = theInput.read() ) != -1 ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						if ( count % 8 != 0 ) {
							System.out.print( " " );
						}
						else {
							if ( count != 0 ) {
								System.out.println();
							}
							System.out.print( HorizAlignTextBuilder.asAligned( count + ": ", 7, '0', HorizAlignTextMode.RIGHT ) );
						}
						System.out.print( NumericalUtility.toHexString( (byte) eValue ) );
					}
					count++;
				}
				fail( "Expected an <" + TimeoutIOException.class.getName() + "> exception!" );
			}
			catch ( TimeoutIOException e ) {
				final float duration = ( System.currentTimeMillis() - startTime ) / 1000;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "\nTimeout after <" + duration + "> seconds: " + e.getMessage() );
				}
				assertEquals( 1, duration );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static class BlockingRandomInputStream extends InputStream {
		private int _length;
		private final long _timeoutMillis;
		private final Random _rnd = new Random();

		public BlockingRandomInputStream( int aLength, long aTimeoutMillis ) {
			_length = aLength;
			_timeoutMillis = aTimeoutMillis;
		}

		@Override
		public int read() throws IOException {
			if ( _length == 0 ) {
				synchronized ( this ) {
					try {
						wait( _timeoutMillis );
					}
					catch ( InterruptedException e ) {}
				}
				return -1;
			}
			_length--;
			return _rnd.nextInt( 0xFF );
		}
	}
}
