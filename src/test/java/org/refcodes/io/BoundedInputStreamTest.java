// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class BoundedInputStreamTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testBoundedInputStream() throws IOException {
		final StringBuilder theBuilder = new StringBuilder();
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		int count = 1;
		try ( BoundedInputStream theBoundedInputStream = new BoundedInputStream( theInptuStream, 10 ) ) {
			int eRead = theBoundedInputStream.read();
			while ( eRead != -1 ) {
				theBuilder.append( (char) eRead );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.print( (char) eRead );
				}
				eRead = theBoundedInputStream.read();
				count++;
			}
		}
		catch ( IOException expected ) {}
		assertEquals( 10, count );
		assertEquals( "1234567890", theBuilder.toString() );
	}

	@Test
	public void testMarkReset() throws IOException {
		final StringBuilder theBuilder = new StringBuilder();
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		int count = 1;
		try ( BoundedInputStream theBoundedInputStream = new BoundedInputStream( theInptuStream, 30 ) ) {
			int eRead;
			// Read first 10 bytes |-->
			for ( int i = 0; i < 10; i++ ) {
				eRead = theBoundedInputStream.read();
				theBuilder.append( (char) eRead );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.print( (char) eRead );
				}
				count++;
			}
			// Read first 10 bytes <--|
			// Skip & revert next 15 bytes |-->
			theBoundedInputStream.mark( 100 );
			for ( int i = 0; i < 15; i++ ) {
				theBoundedInputStream.read();
			}
			theBoundedInputStream.reset();
			// Skip & revert next 15 bytes <--|
			eRead = theBoundedInputStream.read();
			while ( eRead != -1 ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.print( (char) eRead );
				}
				theBuilder.append( (char) eRead );
				eRead = theBoundedInputStream.read();
				count++;
			}
		}
		catch ( IOException expected ) {}
		assertEquals( 30, count );
		assertEquals( "1234567890abcdefghij1234567890", theBuilder.toString() );
	}
}
