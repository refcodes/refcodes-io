// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class InvertibleStreamTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@SuppressWarnings("resource")
	@Test
	public void tesInvertibleStream1() throws IOException {
		final String theInputText = "Hallo Welt!!!";
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final BijectiveOutputStream theInvertibleBijectiveOutputStream = new BijectiveOutputStream( theOutputStream, ( x ) -> (byte) ( x ^ 0x100 ) );
		theInvertibleBijectiveOutputStream.write( theInputText.getBytes() );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		final InverseInputStream theInvertibleInverseInputStream = new InverseInputStream( theInputStream, ( x ) -> (byte) ( x ^ 0x100 ) );
		// byte[] theResult = theInvertibleInverseInputStream.readNBytes( theInvertibleInverseInputStream.available() );
		final byte[] theResult = new byte[theInvertibleInverseInputStream.available()];
		theInvertibleInverseInputStream.read( theResult );
		final String theOutputText = new String( theResult );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theInputText + " ?= " + theOutputText );
		}
		assertEquals( theInputText, theOutputText );
	}

	@SuppressWarnings("resource")
	@Test
	public void tesInvertibleStream2() throws IOException {
		final String theInputText = "Hallo Welt!!!";
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final BijectiveOutputStream theInvertibleBijectiveOutputStream = new BijectiveOutputStream( theOutputStream, ( x ) -> (byte) ( x ^ 0x100 ) );
		theInvertibleBijectiveOutputStream.write( theInputText.getBytes() );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		final InverseInputStream theInvertibleInverseInputStream = new InverseInputStream( theInputStream, ( x ) -> (byte) ( x ^ 0x101 ) );
		// byte[] theResult = theInvertibleInverseInputStream.readNBytes( theInvertibleInverseInputStream.available() );
		final byte[] theResult = new byte[theInvertibleInverseInputStream.available()];
		theInvertibleInverseInputStream.read( theResult );
		final String theOutputText = new String( theResult );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theInputText + " ?= " + theOutputText );
		}
		assertNotEquals( theInputText, theOutputText );
	}
}
