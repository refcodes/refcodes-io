// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;
import java.net.MalformedURLException;
import java.net.URL;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Delimiter;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;

public class FileUtilityTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void toJarHierarchy() throws MalformedURLException {
		final URL theJarPath1 = new URL( "jar:file:/home/steiner/Workspaces/com.fightclub/fightclub-app/target/fightclub-app-0.0.1-SNAPSHOT.jar!/webapp/home.xhtml" );
		final URL theJarPath2 = new URL( "jar:file:/home/steiner/Workspaces/com.fightclub/fightclub-app/target/fightclub-app-0.0.1-SNAPSHOT.jar!/lib/fightclub-adapter-web-0.0.1-SNAPSHOT.jar!/webapp/home.xhtml" );
		String[] theJarHierarchy;
		String theJarPath;
		theJarHierarchy = FileUtility.toJarHierarchy( theJarPath1 );
		theJarPath = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theJarHierarchy ).withDelimiter( Delimiter.PATH.getChar() ).toRecord();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "JAR parh 1 := " + theJarPath );
		}
		assertEquals( "fightclub-app-0.0.1-SNAPSHOT.jar", theJarPath );
		theJarHierarchy = FileUtility.toJarHierarchy( theJarPath2 );
		theJarPath = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theJarHierarchy ).withDelimiter( Delimiter.PATH.getChar() ).toRecord();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "JAR parh 2 := " + theJarPath );
		}
		assertEquals( "fightclub-app-0.0.1-SNAPSHOT.jar/fightclub-adapter-web-0.0.1-SNAPSHOT.jar", theJarPath );
	}
}