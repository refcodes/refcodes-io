// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.io.Serializable;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * Tests the {@link LoopbackDatagramsTransceiver} and the parts
 * {@link LoopbackDatagramsReceiver} and {@link LoopbackDatagramsTransmitter}.
 */
public class LoopbackConnectionTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String DATAGRAM_A = "Datagram A";
	private static final String DATAGRAM_B = "Datagram B";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Tests the {@link LoopbackDatagramsTransceiver}.
	 *
	 * @throws IOException the open exception
	 * @throws InterruptedException the interrupted exception
	 * @throws IOException the close exception
	 */
	@Test
	public void testLoopbackTransceiver() throws IOException, InterruptedException {
		for ( int i = 0; i < 2; i++ ) {
			// OPEN:
			final LoopbackDatagramsTransceiver<Serializable> theLoopbackTransceiverA = new LoopbackDatagramsTransceiver<>();
			final LoopbackDatagramsTransceiver<Serializable> theLoopbackTransceiverB = new LoopbackDatagramsTransceiver<>();
			theLoopbackTransceiverA.open( theLoopbackTransceiverB );
			theLoopbackTransceiverB.open( theLoopbackTransceiverA );
			// WRITE FROM A TO B:
			theLoopbackTransceiverA.transmit( DATAGRAM_A );
			final String theDatagramA = (String) theLoopbackTransceiverB.receive();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "DatagramsTransceiver B read datagram \"" + theDatagramA + "\" from transceiver A." );
			}
			assertEquals( DATAGRAM_A, theDatagramA );
			// WRITE FROM B TO A:
			theLoopbackTransceiverB.transmit( DATAGRAM_B );
			final String theDatagramB = (String) theLoopbackTransceiverA.receive();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "DatagramsTransceiver A read datagram \"" + theDatagramB + "\" from transceiver B." );
			}
			assertEquals( DATAGRAM_B, theDatagramB );
			// CLOSE:
			if ( i == 0 ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Closing transceiver A." );
				}
				theLoopbackTransceiverA.close();
				assertTrue( theLoopbackTransceiverA.isClosed() );
			}
			if ( i == 1 ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Closing transceiver B." );
				}
				theLoopbackTransceiverB.close();
				assertTrue( theLoopbackTransceiverB.isClosed() );
			}
			try {
				theLoopbackTransceiverA.transmit( DATAGRAM_A );
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
			try {
				theLoopbackTransceiverA.receive();
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
			try {
				theLoopbackTransceiverB.transmit( DATAGRAM_A );
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
			try {
				theLoopbackTransceiverB.receive();
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
		}
	}

	@Test
	public void testLoopbackSenderAndReceiver() throws IOException, InterruptedException {
		for ( int i = 0; i < 2; i++ ) {
			// OPEN:
			final LoopbackDatagramsReceiver<Serializable> theLoopbackReceiver = new LoopbackDatagramsReceiver<>();
			final LoopbackDatagramsTransmitter<Serializable> theLoopbackSender = new LoopbackDatagramsTransmitter<>();
			theLoopbackReceiver.open( theLoopbackSender );
			theLoopbackSender.open( theLoopbackReceiver );
			// WRITE A AND B:
			theLoopbackSender.transmit( DATAGRAM_A );
			theLoopbackSender.transmit( DATAGRAM_B );
			String theDatagram = (String) theLoopbackReceiver.receive();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "DatagramsReceiver read datagram \"" + theDatagram + "\" from sender." );
			}
			assertEquals( DATAGRAM_A, theDatagram );
			theDatagram = (String) theLoopbackReceiver.receive();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "DatagramsReceiver read datagram \"" + theDatagram + "\" from sender." );
			}
			assertEquals( DATAGRAM_B, theDatagram );
			// CLOSE:
			if ( i == 0 ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Closing sender." );
				}
				theLoopbackSender.close();
				assertTrue( theLoopbackSender.isClosed() );
			}
			if ( i == 1 ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Closing receiver." );
				}
				theLoopbackReceiver.close();
				assertTrue( theLoopbackReceiver.isClosed() );
			}
			try {
				theLoopbackSender.transmit( DATAGRAM_A );
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
			try {
				theLoopbackReceiver.receive();
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
		}
	}

	@Test
	public void testLoopbackByteSenderAndReceiver() throws IOException, InterruptedException {
		for ( int i = 0; i < 2; i++ ) {
			// OPEN:
			final LoopbackBytesReceiver theLoopbackReceiver = new LoopbackBytesReceiver();
			final LoopbackBytesTransmitter theLoopbackSender = new LoopbackBytesTransmitter();
			theLoopbackReceiver.open( theLoopbackSender );
			theLoopbackSender.open( theLoopbackReceiver );
			// WRITE A AND B:
			theLoopbackSender.transmitBytes( DATAGRAM_A.getBytes() );
			theLoopbackSender.transmitBytes( DATAGRAM_B.getBytes() );
			String theDatagram = new String( theLoopbackReceiver.receiveBytes( DATAGRAM_A.getBytes().length ) );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "DatagramsReceiver read datagram \"" + theDatagram + "\" from sender." );
			}
			assertEquals( DATAGRAM_A, theDatagram );
			theDatagram = new String( theLoopbackReceiver.receiveBytes( DATAGRAM_B.getBytes().length ) );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "DatagramsReceiver read datagram \"" + theDatagram + "\" from sender." );
			}
			assertEquals( DATAGRAM_B, theDatagram );
			// CLOSE:
			if ( i == 0 ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Closing sender." );
				}
				theLoopbackSender.close();
				assertTrue( theLoopbackSender.isClosed() );
			}
			if ( i == 1 ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Closing receiver." );
				}
				theLoopbackReceiver.close();
				assertTrue( theLoopbackReceiver.isClosed() );
			}
			try {
				theLoopbackSender.transmitBytes( DATAGRAM_A.getBytes() );
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
			try {
				theLoopbackReceiver.receiveByte();
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
		}
	}

	@Test
	public void testLoopbackShortSenderAndReceiver() throws IOException, InterruptedException {
		for ( int i = 0; i < 2; i++ ) {
			// OPEN:
			final LoopbackShortsReceiver theLoopbackReceiver = new LoopbackShortsReceiver();
			final LoopbackShortsTransmitter theLoopbackSender = new LoopbackShortsTransmitter();
			theLoopbackReceiver.open( theLoopbackSender );
			theLoopbackSender.open( theLoopbackReceiver );
			// WRITE A AND B:
			theLoopbackSender.transmitShort( (short) 51 );
			theLoopbackSender.transmitShort( (short) 61 );
			short theDatagram = theLoopbackReceiver.receiveShort();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "DatagramsReceiver read datagram \"" + theDatagram + "\" from sender." );
			}
			assertEquals( 51, theDatagram );
			theDatagram = theLoopbackReceiver.receiveShort();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "DatagramsReceiver read datagram \"" + theDatagram + "\" from sender." );
			}
			assertEquals( 61, theDatagram );
			// CLOSE:
			if ( i == 0 ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Closing sender." );
				}
				theLoopbackSender.close();
				assertTrue( theLoopbackSender.isClosed() );
			}
			if ( i == 1 ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Closing receiver." );
				}
				theLoopbackReceiver.close();
				assertTrue( theLoopbackReceiver.isClosed() );
			}
			try {
				theLoopbackSender.transmitShort( (short) 51 );
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
			try {
				theLoopbackReceiver.receiveShort();
				fail( "Should not reach this code!" );
			}
			catch ( IOException aException ) { /* Expected */}
		}
	}
}
