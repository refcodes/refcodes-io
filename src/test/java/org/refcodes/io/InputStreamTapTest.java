// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class InputStreamTapTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testInputStreamTap() throws IOException {
		byte[] theBytes = "Hello World!".getBytes();
		ByteArrayInputStream theInputStream = new ByteArrayInputStream( theBytes );
		ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		try ( InputStreamTap theTap = new InputStreamTap( theInputStream, theOutputStream ) ) {
			byte[] theRead = theTap.readAllBytes();
			byte[] theWritten = theOutputStream.toByteArray();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Read := " + new String( theRead ) );
				System.out.println( "Written := " + new String( theWritten ) );
			}
			assertArrayEquals( theBytes, theRead );
			assertArrayEquals( theBytes, theWritten );
		}
	}
}
