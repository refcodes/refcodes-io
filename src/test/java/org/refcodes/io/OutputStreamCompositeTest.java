// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class OutputStreamCompositeTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testOutputStreamComposite() throws IOException {
		byte[] theBytes = "Hello World!".getBytes();
		ByteArrayOutputStream theOutputStream1 = new ByteArrayOutputStream();
		ByteArrayOutputStream theOutputStream2 = new ByteArrayOutputStream();
		try ( OutputStreamComposite theOutputStream = new OutputStreamComposite( theOutputStream1, theOutputStream2 ) ) {
			theOutputStream.write( theBytes );
			byte[] theWritten1 = theOutputStream1.toByteArray();
			byte[] theWritten2 = theOutputStream2.toByteArray();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Written 1 := " + new String( theWritten1 ) );
				System.out.println( "Written 2 := " + new String( theWritten2 ) );
			}
			assertArrayEquals( theBytes, theWritten1 );
			assertArrayEquals( theBytes, theWritten2 );
		}
	}
}
