// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class ReplaceInputStreamTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testReplace() throws IOException {
		final String theFind = "world";
		final String theReplace = "universe";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come!";
		final String theExpectedText = theStreamText.replace( theFind, theReplace );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		String theProducedText = "";
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( theByteInput, theFind, theReplace ) ) {
			int e;
			while ( ( e = theReplaceInput.read() ) != -1 ) {
				theProducedText += (char) e;
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}

	@Test
	public void testReplace1() throws IOException {
		final String theFind = "world";
		final String theReplace = "universe";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come!";
		final String theExpectedText = theStreamText.replace( theFind, theReplace );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( theByteInput, theFind, theReplace ) ) {
			final ByteArrayOutputStream theByteOutput = new ByteArrayOutputStream();
			theReplaceInput.transferTo( theByteOutput );
			final String theProducedText = theByteOutput.toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}

	@Test
	public void testReplace2() throws IOException {
		final String theFind = " world";
		final String theReplace = "";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come!";
		final String theExpectedText = theStreamText.replace( theFind, theReplace );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( theByteInput, theFind, theReplace ) ) {
			final ByteArrayOutputStream theByteOutput = new ByteArrayOutputStream();
			theReplaceInput.transferTo( theByteOutput );
			final String theProducedText = theByteOutput.toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}

	@Test
	public void testReplace3() throws IOException {
		final String theFind = "world";
		final String theReplace = "universe";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come! You are the world champion!";
		final String theExpectedText = theStreamText.replace( theFind, theReplace );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( theByteInput, theFind, theReplace ) ) {
			final ByteArrayOutputStream theByteOutput = new ByteArrayOutputStream();
			theReplaceInput.transferTo( theByteOutput );
			final String theProducedText = theByteOutput.toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}

	@Test
	public void testReplace4() throws IOException {
		final String theFind = " world";
		final String theReplace = "";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come! You are the world champion!";
		final String theExpectedText = theStreamText.replace( theFind, theReplace );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( theByteInput, theFind, theReplace ) ) {
			final ByteArrayOutputStream theByteOutput = new ByteArrayOutputStream();
			theReplaceInput.transferTo( theByteOutput );
			final String theProducedText = theByteOutput.toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}

	@Test
	public void testReplace5() throws IOException {
		final String theFind = "xyz";
		final String theReplace = "abcdefgh";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come! You are the world champion!";
		final String theExpectedText = theStreamText.replace( theFind, theReplace );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( theByteInput, theFind, theReplace ) ) {
			final ByteArrayOutputStream theByteOutput = new ByteArrayOutputStream();
			theReplaceInput.transferTo( theByteOutput );
			final String theProducedText = theByteOutput.toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}

	@Test
	public void testReplace6() throws IOException {
		final String theFind = " ";
		final String theReplace = "";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come! You are the world champion!";
		final String theExpectedText = theStreamText.replace( theFind, theReplace );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( theByteInput, theFind, theReplace ) ) {
			final ByteArrayOutputStream theByteOutput = new ByteArrayOutputStream();
			theReplaceInput.transferTo( theByteOutput );
			final String theProducedText = theByteOutput.toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}

	@Test
	public void testReplace7() throws IOException {
		final String theFind = " worlD";
		final String theReplace = "XXXX";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come! You are the world champion!";
		final String theExpectedText = theStreamText.replace( theFind, theReplace );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( theByteInput, theFind, theReplace ) ) {
			final ByteArrayOutputStream theByteOutput = new ByteArrayOutputStream();
			theReplaceInput.transferTo( theByteOutput );
			final String theProducedText = theByteOutput.toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}

	@Test
	public void testNested1() throws IOException {
		final String theFind1 = " ";
		final String theReplace1 = "";
		final String theFind2 = "!";
		final String theReplace2 = "";
		final String theFind3 = ",";
		final String theReplace3 = "";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come! You are the world champion!";
		final String theExpectedText = theStreamText.replace( theFind1, theReplace1 ).replace( theFind2, theReplace2 ).replace( theFind3, theReplace3 );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( new ReplaceInputStream( new ReplaceInputStream( theByteInput, theFind3.getBytes(), theReplace3.getBytes() ), theFind2.getBytes(), theReplace2.getBytes() ), theFind1.getBytes(), theReplace1.getBytes() ) ) {
			final ByteArrayOutputStream theByteOutput = new ByteArrayOutputStream();
			theReplaceInput.transferTo( theByteOutput );
			final String theProducedText = theByteOutput.toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}

	@Test
	public void markReplace1() throws IOException {
		final String theFind = "world";
		final String theReplace = "universe";
		final String theStreamText = "Hallo Welt! Hello world, the best is yet to come! You are the world champion!";
		final String theExpectedText = theStreamText.replace( theFind, theReplace );
		final ByteArrayInputStream theByteInput = new ByteArrayInputStream( theStreamText.getBytes() );
		try ( ReplaceInputStream theReplaceInput = new ReplaceInputStream( theByteInput, theFind, theReplace ) ) {
			final ByteArrayOutputStream theByteOutput = new ByteArrayOutputStream();
			int eRead;
			for ( int i = 0; i < 22; i++ ) {
				eRead = theReplaceInput.read();
				theByteOutput.write( eRead );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.print( (char) eRead );
				}
			}
			theReplaceInput.mark( 1024 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.print( "/*MARK!*/" );
			}
			for ( int i = 0; i < 17; i++ ) {
				eRead = theReplaceInput.read();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.print( (char) eRead );
				}
			}
			theReplaceInput.reset();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.print( "/*RESET!*/" );
			}
			while ( ( eRead = theReplaceInput.read() ) != -1 ) {
				theByteOutput.write( eRead );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.print( (char) eRead );
				}
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
			}
			final String theProducedText = theByteOutput.toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "  Stream text: " + theStreamText );
				System.out.println( "Expected text: " + theExpectedText );
				System.out.println( "Produced text: " + theProducedText );
				System.out.println();
			}
			assertEquals( theExpectedText, theProducedText );
		}
	}
}
