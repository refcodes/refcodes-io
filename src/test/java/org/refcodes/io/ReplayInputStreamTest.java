// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class ReplayInputStreamTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testReplayInputStream1() throws IOException {
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		final byte[] theExpected = "1234567890abcdef".getBytes( StandardCharsets.US_ASCII );
		final byte[] theBytes = new byte[16];
		final byte[] theReset = new byte[16];
		try ( ReplayInputStream theReplayInputStream = new ReplayInputStream( theInptuStream ) ) {
			theReplayInputStream.mark( 128 );
			theReplayInputStream.read( theBytes );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theBytes ) );
			}
			theReplayInputStream.reset();
			theReplayInputStream.read( theReset );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theReset ) );
			}
		}
		assertArrayEquals( theBytes, theExpected );
		assertArrayEquals( theBytes, theReset );
	}

	@Test
	public void testReplayInputStream2() throws IOException {
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		final byte[] theBytes = new byte[16];
		try ( ReplayInputStream theReplayInputStream = new ReplayInputStream( theInptuStream ) ) {
			theReplayInputStream.mark( 15 );
			theReplayInputStream.read( theBytes );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theBytes ) );
			}
			theReplayInputStream.reset();
			fail( "Expecting an <" + IOException.class.getSimpleName() + ">!" );
		}
		catch ( IOException expected ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( expected.getMessage() );
			}
		}
	}

	@Test
	public void testReplayInputStream3() throws IOException {
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		final byte[] theExpected = "1234567890abcdef".getBytes( StandardCharsets.US_ASCII );
		final byte[] theBytes = new byte[16];
		final byte[] theReset = new byte[16];
		try ( ReplayInputStream theReplayInputStream = new ReplayInputStream( theInptuStream ) ) {
			theReplayInputStream.mark( 16 );
			theReplayInputStream.read( theBytes );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theBytes ) );
			}
			theReplayInputStream.reset();
			theReplayInputStream.read( theReset );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theReset ) );
			}
		}
		assertArrayEquals( theBytes, theExpected );
		assertArrayEquals( theBytes, theReset );
	}

	@Test
	public void testReplayInputStream4() throws IOException {
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		final byte[] theExpected = "1234567890abcdef".getBytes( StandardCharsets.US_ASCII );
		final byte[] theBytes = new byte[16];
		final byte[] theReset = new byte[16];
		try ( ReplayInputStream theReplayInputStream = new ReplayInputStream( theInptuStream ) ) {
			theReplayInputStream.mark( 17 );
			theReplayInputStream.read( theBytes );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theBytes ) );
			}
			theReplayInputStream.reset();
			theReplayInputStream.read( theReset );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theReset ) );
			}
		}
		assertArrayEquals( theBytes, theExpected );
		assertArrayEquals( theBytes, theReset );
	}

	@Test
	public void testReplayInputStream5() throws IOException {
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		final byte[] theBytes = new byte[16];
		try ( ReplayInputStream theReplayInputStream = new ReplayInputStream( theInptuStream ) ) {
			theReplayInputStream.mark( 12 );
			theReplayInputStream.read( theBytes );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theBytes ) );
			}
			theReplayInputStream.reset();
			fail( "Expecting an <" + IOException.class.getSimpleName() + ">!" );
		}
		catch ( IOException expected ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( expected.getMessage() );
			}
		}
	}

	@Test
	public void testReplayInputStream6() throws IOException {
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		final byte[] theExpected = "1234567890abcdef".getBytes( StandardCharsets.US_ASCII );
		final byte[] theBytes = new byte[16];
		try ( ReplayInputStream theReplayInputStream = new ReplayInputStream( theInptuStream ) ) {
			theReplayInputStream.mark( 6 );
			theReplayInputStream.read( theBytes );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theBytes ) );
			}
		}
		assertArrayEquals( theBytes, theExpected );
	}

	@Test
	public void testReplayInputStream7() throws IOException {
		final StringBuilder theBuilder = new StringBuilder();
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		try ( ReplayInputStream theReplayInputStream = new ReplayInputStream( theInptuStream ) ) {
			theReplayInputStream.mark( 1024 );
			int eRead = theReplayInputStream.read();
			while ( eRead != -1 ) {
				theBuilder.append( (char) eRead );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.print( (char) eRead );
				}
				eRead = theReplayInputStream.read();
			}
		}
		assertEquals( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij", theBuilder.toString() );
	}

	@Test
	public void testReplayInputStream8() throws IOException {
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij".getBytes() );
		final byte[] theExpected = "1234567890abcdef".getBytes( StandardCharsets.US_ASCII );
		final byte[] theBytes = new byte[10];
		final byte[] theReset = new byte[16];
		try ( ReplayInputStream theReplayInputStream = new ReplayInputStream( theInptuStream ) ) {
			theReplayInputStream.mark( 128 );
			theReplayInputStream.read( theBytes );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theBytes ) );
			}
			theReplayInputStream.reset();
			theReplayInputStream.read( theReset );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new String( theReset ) );
			}
		}
		assertArrayEquals( theExpected, theReset );
	}

	@Test
	public void testReplayInputStream9() throws IOException {
		final ByteArrayInputStream theInptuStream = new ByteArrayInputStream( "1234567890".getBytes() );
		final byte[] theBytes = new byte[16];
		try ( ReplayInputStream theReplayInputStream = new ReplayInputStream( theInptuStream ) ) {
			final int length = theReplayInputStream.read( theBytes );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( length + " --> " + new String( theBytes ) + " --> " + Arrays.toString( theBytes ) );
			}
		}
	}
}
